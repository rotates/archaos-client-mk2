package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import org.gestouch.core.Gestouch;
	import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
	import org.gestouch.extensions.starling.StarlingTouchHitTester;
	import org.gestouch.input.NativeInputAdapter;
	import org.rotates.archaos.Game;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.textures.TextureSmoothing;
	
	//[SWF(width='1024', height='768', backgroundColor='#000000', frameRate='60')] // desktop / iPad 1 / 2 / mini (landscape)
	//[SWF(width='480', height='320', backgroundColor='#000000', frameRate='60')] // iPhone 3GS (landscape)
	//[SWF(width='960', height='640', backgroundColor='#000000', frameRate='60')] // iPhone 4 / 4S (landscape)
	//[SWF(width='1136', height='640', backgroundColor='#000000', frameRate='60')] // iPhone 5 (landscape)
	
	//[SWF(width='768', height='1024', backgroundColor='#000000', frameRate='60')] // iPad 1 / 2 / mini (portrait)
	//[SWF(width='320', height='480', backgroundColor='#000000', frameRate='60')] // iPhone 3GS (portrait)
	[SWF(width='640', height='960', backgroundColor='#000000', frameRate='60')] // iPhone 4 / 4S (portrait)
	//[SWF(width='640', height='1136', backgroundColor='#000000', frameRate='60')] // iPhone 5 (portrait)
	
	public class Main extends Sprite
	{
		public static const DEBUG:Boolean = false;
		public static const MOBILE:Boolean = true;
		public static const SCALE_DIVISOR:Number = 2;
		public static const ASSET_SCALE_MULTIPLIER:uint = 1;
		public static const TEXTURE_SMOOTHING:String = TextureSmoothing.NONE;
		
		private var viewportRect:Rectangle;

		private var game:Starling;
		
		public function Main()
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void
		{
			viewportRect = new Rectangle(0,0,stage.stageWidth << 0, stage.stageHeight << 0);
			
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(Event.RESIZE, onResize);
			
			stage.quality=StageQuality.LOW;
			stage.scaleMode=StageScaleMode.NO_SCALE;
			stage.align=StageAlign.TOP_LEFT;
			
			Starling.handleLostContext = true;
						
			game = new Starling(Game, stage, viewportRect, null, "auto", "baselineConstrained" );
			
			game.stage.stageWidth = (stage.stageWidth / SCALE_DIVISOR) << 0;
			game.stage.stageHeight = (stage.stageHeight / SCALE_DIVISOR) << 0;

			Gestouch.inputAdapter||=new NativeInputAdapter(stage);
			Gestouch.addDisplayListAdapter(starling.display.DisplayObject, new StarlingDisplayListAdapter());
			Gestouch.addTouchHitTester(new StarlingTouchHitTester(game), -1);
			
			if (DEBUG) {
				game.showStats = true;
			}
			game.start();
			onResize();
		}
		
		private function onResize(e:Event = null):void
		{
			viewportRect.width = stage.stageWidth << 0;
			viewportRect.height = stage.stageHeight << 0;
			game.viewPort = viewportRect;
			game.stage.stageWidth = (stage.stageWidth / SCALE_DIVISOR) << 0;
			game.stage.stageHeight = (stage.stageHeight / SCALE_DIVISOR) << 0;
		}
	}
}
package assets
{
	public class EmbeddedAssets
	{	
		/*
		// board
		[Embed(source = "board/tiles.png")]
		public static const tiles:Class;
		[Embed(source = "board/tiles.xml", mimeType = "application/octet-stream")]
		public static const tilesXML:Class;
		
		// units
		[Embed(source = "units/classicunits.png")]
		public static const classicunits:Class;
		[Embed(source = "units/classicunits.xml", mimeType = "application/octet-stream")]
		public static const classicunitsXML:Class;
		
		[Embed(source = "units/shadow.png")]
		public static const shadow:Class;
		
		[Embed(source = "units/wizards.png")]
		public static const wizards:Class;
		
		[Embed(source = "units/hats.png")]
		public static const hats:Class;
		
		// UI
		[Embed(source = "ui/ui.png")]
		public static const ui:Class;
		[Embed(source = "ui/ui.xml", mimeType = "application/octet-stream")]
		public static const uiXML:Class;
		*/
		
		// 1x
		[Embed(source = "gfx.png")]
		public static const gfx:Class;
		[Embed(source = "gfx.xml", mimeType = "application/octet-stream")]
		public static const gfxXML:Class;	
		
		// fonts
		[Embed(source = "fonts/archaosfont.png")]
		public static const archaosfont:Class;
		[Embed(source = "fonts/archaosfont.xml", mimeType = "application/octet-stream")]
		public static const archaosfontXML:Class;
		
		[Embed(source = "fonts/chaosfont.png")]
		public static const chaosfont:Class;
		[Embed(source = "fonts/chaosfont.xml", mimeType = "application/octet-stream")]
		public static const chaosfontXML:Class;
		
		/*
		// 2x
		[Embed(source = "2x/gfx.png")]
		public static const gfx:Class;
		[Embed(source = "2x/gfx.xml", mimeType = "application/octet-stream")]
		public static const gfxXML:Class;	
		
		// fonts
		[Embed(source = "2x/fonts/archaosfont.png")]
		public static const archaosfont:Class;
		[Embed(source = "2x/fonts/archaosfont.xml", mimeType = "application/octet-stream")]
		public static const archaosfontXML:Class;
		
		[Embed(source = "2x/fonts/chaosfont.png")]
		public static const chaosfont:Class;
		[Embed(source = "2x/fonts/chaosfont.xml", mimeType = "application/octet-stream")]
		public static const chaosfontXML:Class;
		*/
		
		// sounds
		[Embed(source = "sounds/select.mp3")]
		public static const sndSelect:Class;
		
		[Embed(source = "sounds/cancel.mp3")]
		public static const sndCancel:Class;
		
		[Embed(source = "sounds/endturn.mp3")]
		public static const sndEndTurn:Class;
		
		[Embed(source = "sounds/move.mp3")]
		public static const sndMove:Class;
		
		[Embed(source = "sounds/fly.mp3")]
		public static const sndFly:Class;
		
		[Embed(source = "sounds/attack.mp3")]
		public static const sndAttack:Class;
		
		[Embed(source = "sounds/rangedattack.mp3")]
		public static const sndRangedAttack:Class;
		
		[Embed(source = "sounds/kill.mp3")]
		public static const sndKill:Class;
		
		[Embed(source = "sounds/engaged.mp3")]
		public static const sndEngaged:Class;

		
	}
}
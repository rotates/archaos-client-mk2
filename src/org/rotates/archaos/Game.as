package org.rotates.archaos
{
	import assets.EmbeddedAssets;
	
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.ScreenNavigator;
	import feathers.controls.ScreenNavigatorItem;
	import feathers.controls.TextInput;
	import feathers.layout.VerticalLayout;
	
	import org.osmf.logging.Log;
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.phases.RuleManager;
	import org.rotates.archaos.board.renderers.WizardTexture;
	import org.rotates.archaos.net.NetManager;
	import org.rotates.archaos.ui.ActiveGame;
	import org.rotates.archaos.ui.Browser;
	import org.rotates.archaos.ui.Login;
	import org.rotates.archaos.ui.NewGame;
	import org.rotates.archaos.ui.RoundedScreenSlidingStackTransitionManager;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	
	public class Game extends Sprite
	{
		public static const ASSETS:AssetManager = new AssetManager();
		public static const NET:NetManager = new NetManager("82.4.14.208", NetManager.TCP);
		public static const PLAYER_COLORS:Vector.<uint> = new <uint>[0xff0000, 0x00ffff, 0xff7c00, 0x007cff, 0xffff00, 0x00ff00, 0xffffff, 0xff00ff];
		
		public static const LOGIN:String = "login";
		public static const BROWSER:String = "browser";
		public static const NEWGAME:String = "newgame"; 
		public static const ACTIVEGAME:String = "activegame";
		
		private static var loginScreen:Login;
		private static var browserScreen:Browser;
		private static var newGameScreen:NewGame;
		private static var activeGameScreen:ActiveGame;
		
		public static var username:String;
		public static var handle:String;
		public static var token:String;
		public static var navigator:ScreenNavigator = new ScreenNavigator();
		
		public static var boards:Vector.<Board> = new Vector.<Board>();
		
		public static var unitDefs:Object;
		
		public function Game()
		{
			ASSETS.verbose = true;
			ASSETS.enqueue(EmbeddedAssets);
			ASSETS.loadQueue(function(ratio:Number):void {
				if (ratio == 1.0) {
					init();
					showLogin();
				}
			});
		}
		
		public static function addBoard(id:String, data:Object):Board
		{
			var board:Board = getBoard(id);
			if (board === null) {
				board = new Board(id, data);
				boards.push(board);
			}
			return board;
		}
		
		public static function getBoard(id:String):Board
		{
			if (boards.length > 0) {
				for (var b:uint = 0; b < boards.length; b++) {
					if (boards[b].id == id) {
						return boards[b];
					}
				}
			}
			return null;
		}
		
		public static function setActiveBoard(id:String):void
		{
			for (var b:uint = 0; b < boards.length; b++) {
				if (boards[b].id === id) {
					boards[b].activeBoard = true;
				}
				else {
					boards[b].activeBoard = false;
				}
			}
		}
		
		public static function getActiveBoard():Board
		{
			for (var b:uint = 0; b < boards.length; b++) {
				if (boards[b].activeBoard === true) {
					return boards[b];
				}
			}
			return null;
		}
		
		public static function showLogin():void
		{
			username = handle = token = null;
			navigator.showScreen(LOGIN);
			// doLogin("lewster32", "bob");
			// doLogin("andyh", "bob");
		}
		
		public static function doLogin(u:String, p:String):void 
		{
			NET.client.login(u, p, function(data:Object):void {
				if (data.response.hasOwnProperty("success")) {
					username = data.response.data.username;
					handle = data.response.data.handle;
					token = data.response.data.token;
					NET.client.getUnitDefs(function(data:Object):void {
						if (data.response.hasOwnProperty("error")) {
							throw new Error("Could not download Unit definitions");
						}
						unitDefs = data.response;
					});
					navigator.showScreen(BROWSER);
					browserScreen.refresh();
				}
				else {
					loginScreen.showError(data.response.text);
				}
			});
		}
		
		private function init():void
		{
			loginScreen = new Login();
			browserScreen = new Browser();
			newGameScreen = new NewGame();
			activeGameScreen = new ActiveGame();
			addChild(navigator);
			var transitionManager:RoundedScreenSlidingStackTransitionManager = new RoundedScreenSlidingStackTransitionManager(navigator);
			transitionManager.duration = 0.5;
			transitionManager.ease = Transitions.EASE_IN_OUT;
			navigator.addScreen(LOGIN, new ScreenNavigatorItem(loginScreen));
			navigator.addScreen(BROWSER, new ScreenNavigatorItem(browserScreen));
			navigator.addScreen(NEWGAME, new ScreenNavigatorItem(newGameScreen));
			navigator.addScreen(ACTIVEGAME, new ScreenNavigatorItem(activeGameScreen));
		}
	}
}
package org.rotates.archaos.board
{	
	import feathers.controls.Button;
	
	import flash.geom.Point;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Gizmos.MoveGizmo;
	import org.rotates.archaos.board.actions.LocalAction;
	import org.rotates.archaos.board.phases.Phase;
	import org.rotates.archaos.board.phases.RuleManager;
	import org.rotates.archaos.board.renderers.BoardPreview;
	import org.rotates.archaos.board.renderers.BoardRenderer;
	import org.rotates.archaos.board.renderers.IBoardRenderer;
	
	public class Board
	{
		public static const STATE_SPECTATING:String = "spectating";
		public static const STATE_SELECTING_UNIT:String = "selecting_unit";
		public static const STATE_MOVING:String = "moving";
		public static const STATE_ENGAGED:String = "engaged";
		public static const STATE_ATTACKING:String = "attacking";
		public static const STATE_RANGED_ATTACKING:String = "ranged_attacking";
		public static const STATE_DISMOUNTING:String = "dismounting";
		public static const STATE_SELECTING_SPELL:String = "selecting_spell";
		public static const STATE_CASTING:String = "casting";
		
		private static const REFRESH_TIME:Number = 10;
				
		private var _id:String;
		private var _name:String;
		private var _players:Vector.<Player> = new Vector.<Player>();
		private var _units:Vector.<Unit> = new Vector.<Unit>();
		private var _active:Boolean;
		private var _ended:Boolean;
		private var _round:uint;
		private var _lastModifed:uint;
		private var _boardWidth:uint;
		private var _boardHeight:uint;
		private var _currentPhase:uint = Phase.PHASE_MOVE;
		private var _currentPlayer:Player;
		private var _currentState:String = Board.STATE_SELECTING_UNIT;
		private var _owner:Player;
		private var _maxPlayers:uint;
		private var _seed:uint;
		
		public var lastUpdated:Number = 0;
		private var _activeBoard:Boolean = false;
		
		private var _renderers:Vector.<IBoardRenderer> = new Vector.<IBoardRenderer>();
		private var _cursor:Cursor;
		private var _actionManager:ActionManager;
		private var _ruleManager:RuleManager;
		private var _selectedUnit:Unit = null;
		private var _gizmos:Object = new Object();
		
		public function Board(id:String, data:Object)
		{
			this._id = id;
			
			_actionManager = new ActionManager(this);
			_ruleManager = new RuleManager(this);
			_gizmos['moveGizmo'] = new MoveGizmo(this);
			
			if (data.hasOwnProperty("name")) {
				this._name = data.name;
			}
			if (data.hasOwnProperty("active")) {
				this._active = data.active;
			}
			if (data.hasOwnProperty("ended")) {
				this._ended = data.ended;
			}
			if (data.hasOwnProperty("round")) {
				this._round = parseInt(data.round, 10);
			}
			if (data.hasOwnProperty("players")) {
				updatePlayers(data.players);	
			}
			if (data.hasOwnProperty("maxPlayers")) {
				_maxPlayers = parseInt(data.maxPlayers, 10);
			}
			if (data.hasOwnProperty("seed")) {
				_seed = parseInt(data.seed, 10);
			}
		}
		
		public function get selectedUnit():Unit
		{
			return _selectedUnit;
		}
		
		public function set selectedUnit(unit:Unit):void
		{
			trace("Selected unit:", unit.id);
			_selectedUnit = unit;
		}
		
		public function get currentState():String
		{
			return _currentState;
		}
		
		public function set currentState(state:String):void
		{
			_currentState = state;
			if (_currentState == Board.STATE_SELECTING_UNIT) {
				_selectedUnit = null;
			}
			if (_currentState == Board.STATE_MOVING) {
				// do range and path drawing routine
			}
			trace("Board state:", _currentState, _selectedUnit ? _selectedUnit.id : null);
		}
		
		public function get currentPhase():uint
		{
			return _currentPhase;
		}
		
		public function set currentPhase(value:uint):void
		{
			currentPhase = value;
		}
		
		public function selectIntent(pos:Point):void
		{
			_ruleManager.selectHandler(pos);
		}
		
		public function selectConfirm(pos:Point):void
		{
			_ruleManager.selectHandler(pos, true);
		}
		
		public function get currentPlayer():Player
		{
			return _currentPlayer;
		}

		public function setCurrentPlayer(player:uint):void
		{
			var newPlayer:Player = getPlayer(player);
			if (newPlayer !== null) {
				_currentPlayer = newPlayer; 
			}
			else {
				throw new Error("Current player must be a valid player");
			}
		}
		
		public function update(callback:Function = null, force:Boolean = false):void
		{
			var board:Board = this;
			if (_units.length == 0 || force === true || (new Date().time - lastUpdated) > (REFRESH_TIME * 1000)) {
				trace("Loading from server");
				Game.NET.client.getGame(_id, function(data:Object):void {
					if (data.response.hasOwnProperty("success")) {
						if (_renderers.length == 0) {
							_boardWidth = data.response.success.width;
							_boardHeight = data.response.success.height;
							_renderers.push(new BoardPreview(_boardWidth,_boardHeight));
							_renderers.push(new BoardRenderer(_boardWidth, _boardHeight));
							_cursor = new Cursor(board, _renderers[1]);
						}
						updateUnits(data.response.success.units);
						updatePlayers(data.response.success.players);
						_round = parseInt(data.response.success.round, 10) || 0;
						_active = data.response.success.active;
						_ended = data.response.success.ended;
						_seed = parseInt(data.response.success.seed, 10);
						_actionManager.clear();
						_actionManager.lastAction = _actionManager.currentAction = data.response.success.lastAction;
						if (data.response.success.hasOwnProperty("currentPlayer")) {
							setCurrentPlayer(data.response.success.currentPlayer);
						}
						if (callback !== null) {
							callback.call();
						}
					}
				});
			}
			else {
				trace("Recalling from memory");
				callback.call();
			}
		}

		private function updatePlayers(players:Array):void
		{
			if (players.length < 1) {
				throw new Error("Invalid player list");
			}
			else {
				this._players = new Vector.<Player>();
				for (var p:uint = 0; p < players.length; p++) {
					this._players.push(new Player(players[p], p));
				}
			}
			_owner = _players[0];
			lastUpdated = new Date().time;
		}
		
		private function updateUnits(units:Object):void
		{
			var unit:Unit;
			this._units = new Vector.<Unit>();
			for (var u:Object in units) {
				units[u].owner = getPlayer(units[u].owner);
				if (units[u].owner) {
					this._units.push(new Unit(units[u]));
				}
			}
			
			for (var i:uint = 0; i < _units.length; i++) {
				if (units[_units[i].id].hasOwnProperty("rider")) {
					_units[i].rider = getUnit(units[_units[i].id].rider);
				}
			}
			
			updateRenderers();
			lastUpdated = new Date().time;
		}
		
		public function updateRenderers():void {
			if (_renderers.length > 0) {
				for (var i:uint = 0; i < _renderers.length; i++) {
					_renderers[i].update(_units);
				}
			}
		}
		
		public function getPlayerByUsername(username:String):Player
		{
			if (this._players.length > 0) {
				for (var p:uint = 0; p < this._players.length; p++) {
					if (this._players[p].username === username) {
						return this._players[p];
					}
				}
			}
			return null;
		}
		
		public function getPlayer(index:uint):Player
		{
			if (this._players.length > 0 && this._players[index]) {
				return this._players[index];
			}
			return null;
		}
		
		public function get players():Vector.<Player>
		{
			return _players;
		}

		public function getWizard(player:Player):Unit
		{
			for (var u:uint = 0; u < _units.length; u++) {
				if (_units[u].isWizard() && _units[u].owner == player) {
					return _units[u];
				}
			}
			return null;
		}
		
		public function getPlayerUnits(player:Player):Vector.<Unit>
		{
			var output:Vector.<Unit> = _units.filter(function(u:Unit):Boolean {
				return (u.owner == player);
			});
			return output;
		}
		
		public function getUnit(id:String):Unit
		{
			for (var u:uint = 0; u < this._units.length; u++) {
				if (this._units[u].id === id) {
					return this._units[u];
				}
			}
			return null;
		}
		
		public function removeUnit(unit:Unit):void
		{
			if (unit) {
				_units.splice(_units.indexOf(unit), 1);
			}
		}
		
		public function getUnitAt(xx:uint, yy:uint):Unit
		{
			for (var u:uint = 0; u < this._units.length; u++) {
				if (this._units[u].position.x == xx && this._units[u].position.y == yy && this._units[u].isRider === false && this._units[u].dead === false) {
					return this._units[u];
				}
			}
			return null;
		}
		
		public function getAdjacent(unit:Unit, test:String = "engageable"):Vector.<Unit>
		{
			var s:Vector.<Array> = new <Array>[[-1,-1], [0,-1], [1, -1], [-1, 0], [1, 0], [-1,1], [0,1], [1, 1]], output:Vector.<Unit> = new Vector.<Unit>(), u:Unit;
			if (unit == null) {
				return output;
			}
			for (var i:uint = 0; i < s.length; i++) {
				u = getUnitAt(unit.position.x+s[i][0], unit.position.y+s[i][1]);
				switch (test) {
					case "engageable":
						if (u && !u.isOwnedBy(unit.owner) && u.properties.mnv > 0 && unit.dead === false) {
							output.push(u);
						}
						break;
					case "enemies":
						if (u && !u.isOwnedBy(unit.owner) && u.dead === false) {
							output.push(u);
						}
						break;
					case "friendlies":
						if (u && !u.isOwnedBy(unit.owner) && u.dead === false) {
							output.push(u);
						}
						break;
					case "attackable":
						if (u && u.dead === false && u.attackableBy(unit)) {
							output.push(u);
						}
						break;
					case "live":
						if (u && u.dead === false) {
							output.push(u);
						}
						break;
					case "dead":
						if (u && u.dead === true) {
							output.push(u);
						}
						break;
					case "all":
						if (u) {
							output.push(u);
						}
						break;
				}
			}
			return output;
		}
		
		public static function getDistance(pt1:Point, pt2:Point):Number
		{
			const diff:Point = new Point(Math.abs(pt1.x - pt2.x), Math.abs(pt1.y - pt2.y));
			return Math.max(diff.x, diff.y) - Math.min(diff.x, diff.y) + Math.min(diff.x, diff.y) * 1.5;
		}
		
		public function get gizmos():Object
		{
			return _gizmos;
		}
		
		public function get cursor():Cursor
		{
			return _cursor;
		}
		
		public function get actionManager():ActionManager
		{
			return _actionManager;
		}
		
		public function get round():uint
		{
			return _round;
		}
		
		public function set round(value:uint):void
		{
			_round = round;
		}

		public function get active():Boolean
		{
			return _active;
		}
		
		public function get ended():Boolean
		{
			return _ended;
		}
		
		public function set ended(value:Boolean):void
		{
			_ended = value;
		}

		public function get lastModifed():uint
		{
			return _lastModifed;
		}

		public function get name():String
		{
			return _name;
		}

		public function get id():String
		{
			return _id;
		}
		
		public function get owner():Player
		{
			return _owner;
		}
		
		public function set owner(value:Player):void
		{
			_owner = value;
		}
		
		public function get maxPlayers():uint {
			return _maxPlayers;
		}

		public function get boardWidth():uint
		{
			return _boardWidth;
		}
		
		public function get boardHeight():uint
		{
			return _boardHeight;
		}
		
		public function get renderers():Vector.<IBoardRenderer>
		{
			return _renderers;
		}
		
		public function set activeBoard(value:Boolean):void
		{
			_activeBoard = value;
			if (_activeBoard === true) {
				_actionManager.tick();
			}
		}
		
		public function get activeBoard():Boolean
		{
			return _activeBoard;
		}
		
		public function get seed():uint
		{
			return _seed;
		}
		
		public function set seed(value:uint):void
		{
			_seed = value;
		}
	}
}
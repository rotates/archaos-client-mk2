package org.rotates.archaos.board
{
	public class Dice
	{
		public function Dice()
		{
			// abstract
		}
		
		public static function random(board:Board):uint
		{
			board.seed = (214013 * board.seed + 2531011) & 0x7fffffff;
			return board.seed / 0x7fffffff;
		}
		
		public static function roll(board:Board, sides:uint = 10):uint
		{
			return Math.floor(random(board) * sides);
		}
		
		public static function rollAgainst(board:Board, atk:uint, def:uint):Boolean
		{
			var atkRoll:uint, defRoll:uint;
			atkRoll = roll(board);
			defRoll = roll(board);
			return (atk + atkRoll >= def + defRoll);
		}
	}
}
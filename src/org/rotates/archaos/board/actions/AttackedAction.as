package org.rotates.archaos.board.actions
{
	import org.rotates.archaos.board.Unit;

	public class AttackedAction implements IAction
	{		
		private const _type:String = LocalAction.ATTACKED;
		private var _unit:Unit;
		private var _target:Unit;
		private var _success:Boolean;
		
		public function AttackedAction(unit:Unit, target:Unit, success:Boolean)
		{
			_unit = unit;
			_target = target;
			_success = success;
		}
		
		public function get object():Object
		{
			return {
				unit: _unit.id,
				target: _target.id,
				success: _success
			}
		}
		
		public function get type():String
		{
			return _type;
		}
	}
}
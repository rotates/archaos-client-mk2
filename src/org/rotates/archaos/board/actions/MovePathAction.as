package org.rotates.archaos.board.actions
{
	import flash.geom.Point;
	
	import org.rotates.archaos.board.Unit;

	public class MovePathAction implements IAction
	{		
		private const _type:String = LocalAction.MOVED;
		private var _unit:Unit;
		private var _path:Vector.<Point>;
		
		public function MovePathAction(unit:Unit)
		{
			_unit = unit;
			_path = unit.path.slice();
		}
		
		public function get object():Object
		{
			var path:Array = new Array();
			for (var p:uint = 0; p < _path.length; p++) {
				path.push({x: _path[p].x, y: _path[p].y});
			}
			return {
				unit: _unit.id,
				path: path
			}
		}
		
		public function get type():String
		{
			return _type;
		}
	}
}
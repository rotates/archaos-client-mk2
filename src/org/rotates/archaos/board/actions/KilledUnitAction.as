package org.rotates.archaos.board.actions
{
	import flash.geom.Point;
	
	import org.rotates.archaos.board.Unit;

	public class KilledUnitAction implements IAction
	{		
		private const _type:String = LocalAction.KILLED;
		private var _unit:Unit;
		
		public function KilledUnitAction(unit:Unit)
		{
			_unit = unit;
		}
		
		public function get object():Object
		{
			return {
				unit: _unit.id
			}
		}
		
		public function get type():String
		{
			return _type;
		}
	}
}
package org.rotates.archaos.board.actions
{
	import flash.geom.Point;
	
	import org.rotates.archaos.board.Unit;

	public class EngagedAction implements IAction
	{		
		private const _type:String = LocalAction.ENGAGED;
		private var _units:Array;
		
		public function EngagedAction(units:*)
		{
			if (units.length > 0) {
				if (units is Vector.<Unit>) {
					_units = new Array();
					for (var u:uint = 0; u < units.length; u++) {
						_units.push(units[u].id);
					}
				}
				else {
					_units = units.slice();
				}
			}
			else {
				throw new Error("Engaged action must specify one or more units");
			}
		}
		
		public function get object():Object
		{
			return {
				units: _units
			}
		}
		
		public function get type():String
		{
			return _type;
		}
	}
}
package org.rotates.archaos.board.actions
{
	public interface IAction
	{
		function get type():String;
		function get object():Object;	
	}
}
package org.rotates.archaos.board.actions
{
	import org.osflash.async.Deferred;
	import org.osflash.async.Promise;
	import org.osflash.async.when;
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.Player;
	import org.rotates.archaos.board.Unit;

	public class LocalAction
	{
		public static const NEW_GAME:String = "newGame";
		public static const GAME_STARTED:String = "gameStarted";
		public static const JOINED:String = "joined";
		public static const LEFT:String = "left";
		public static const NEW_OWNER:String = "newOwner";
		public static const DEFEATED:String = "defeated";
		public static const REMOVED_UNITS:String = "removedUnits";
		public static const END_TURN:String = "endturn";
		public static const ENDED:String = "ended";
		public static const MESSAGE:String = "message";
		public static const MOVED:String = "moved";
		public static const ENGAGED:String = "engaged";
		public static const ATTACKED:String = "attacked";
		public static const RANGED_ATTACKED:String = "rangedAttacked";
		public static const KILLED:String = "killed";
		public static const MOUNTED:String = "mounted";
		public static const DISMOUNTED:String = "dismounted";
		public static const NEW_TURN:String = "newTurn";
		public static const NEW_UNIT:String = "newUnit";
		// to implement
		public static const SELECTED_SPELLS:String = "selectedSpells";
		public static const CAST_SPELL:String = "castSpell";
		
		private var _id:uint;
		private var _type:String;
		private var _data:Object;
		private var _verified:Boolean = false;
		private var _board:Board;
		private var _callback:Function;
		
		public function LocalAction(id:uint, type:String, data:Object, board:Board)
		{
			_id = id;
			_type = type;
			_data = data;
			_board = board;
		}
		
		public function perform(callback:Function):void
		{
			_callback = callback;
			var unit:Unit, target:Unit, player:Player, deferred:Deferred, promises:Vector.<Promise>, u:uint;
			switch (_type) {
				case NEW_GAME:
				case GAME_STARTED:
					_board.update(null, true);
					_callback.call();
					break;
				case JOINED:
					player = _board.getPlayer(_data.username); 
					if (player != null) {
						_board.players.push(new Player(_data, _board.players.length));
					}
					_callback.call();
					break;
				case LEFT:
					player = _board.getPlayer(_data.username); 
					if (player != null) {
						_board.players.splice(_board.players.indexOf(player), 1);
					}
					_callback.call();
					break;
				case NEW_OWNER:
					_board.owner = _board.getPlayer(_data.owner);
					_callback.call();
					break;
				case DEFEATED:
					player = _board.getPlayer(_data.username); 
					player.defeated = true;
					unit = _board.getWizard(player);
					if (unit != null) {
						_board.renderers[1].doEffect("wizardDeath", {unit: unit}, function():void {
							_callback.call();
						});
					}
					else {
						_callback.call();
					}
					break;
				case REMOVED_UNITS:
					promises = new Vector.<Promise>();
					for (u = 0; u < _data.units.length; u++) {
						unit = _board.getUnit(_data.units[u]);
						if (unit != null) {
							promises.push(_board.renderers[1].doEffect("destroyUnit", {unit: unit}));
						}
						_board.removeUnit(unit);
					}
					when(promises).completes(function():void {
						_callback.call();
					});
					break;
				case ENDED:
					_board.ended = true;
					_callback.call();
					break;
				case MESSAGE:
					trace(_board.getPlayer(_data.username).handle + ": " + _data.text);
					_callback.call();
					break;
				case MOVED:
					unit = _board.getUnit(_data.unit);
					if (unit != null) {
						if (_data.hasOwnProperty("to")) {
							unit.move(_data.to.x, _data.to.y, function():void {
								unit.moved = true;
								callback.call();
							});
						}
						else if (_data.hasOwnProperty("path")) {
							unit.movePath(_data.path.slice(), function():void {
								unit.moved = true;
								callback.call();
							});
						}
					}
					else {
						throw new Error("Unit not found: " + _data.unit);
					}
					break;
				case ENGAGED:
					for (u = 0; u < _data.units.length; u++)
					{
						unit = _board.getUnit(_data.units[u]);
						if (unit != null) {
							unit.engaged = true;
						}
					}
					callback.call();
					break;
				case ATTACKED:
					unit = _board.getUnit(_data.unit);
					target = _board.getUnit(_data.target);
					_board.renderers[1].doEffect("attackUnit", {unit: unit, target: target}, function():void {
						unit.attacked = true;
						_callback.call();					
					});
					break;
				case RANGED_ATTACKED:
					unit = _board.getUnit(_data.unit);
					target = _board.getUnit(_data.target);
					promises = new Vector.<Promise>();
					_board.renderers[1].doEffect("attackUnit", {unit: unit, target: target}, function():void {
						unit.rangedAttacked = true;
						_callback.call();					
					});
					break;
				case KILLED:
					unit = _board.getUnit(_data.unit);
					_board.renderers[1].doEffect("attackUnit", {unit: unit, target: target}, function():void {
						unit.dead = true;
						_callback.call();					
					});
					break;
				case NEW_TURN:
					_board.round = _data.round;
					_board.currentPhase = _data.phase;
					_board.setCurrentPlayer(_data.currentPlayer);
					_callback.call();
					break;
			}
		}
		
		public function get id():uint
		{
			return _id;
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function get data():Object
		{
			return _data;
		}
		
		public function get verified():Boolean
		{
			trace("Verified", _id, _type);
			return _verified;
		}
		
		public function verify():void
		{
			_verified = true;
		}
	}
}
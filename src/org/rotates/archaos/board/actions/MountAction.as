package org.rotates.archaos.board.actions
{
	import flash.geom.Point;
	
	import org.rotates.archaos.board.Unit;
	
	public class MountAction implements IAction
	{		
		private const _type:String = LocalAction.MOUNTED;
		private var _unit:Unit;
		private var _target:Unit;
		
		public function MountAction(unit:Unit, target:Unit)
		{
			_unit = unit;
			_target = target;
		}
		
		public function get object():Object
		{
			return {
				unit: _unit.id,
					target: _target.id
			}
		}
		
		public function get type():String
		{
			return _type;
		}
	}
}
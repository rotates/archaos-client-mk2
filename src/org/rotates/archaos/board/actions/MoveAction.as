package org.rotates.archaos.board.actions
{
	import flash.geom.Point;
	
	import org.rotates.archaos.board.Unit;

	public class MoveAction implements IAction
	{		
		private const _type:String = LocalAction.MOVED;
		private var _unit:Unit;
		private var _from:Object;
		private var _to:Object;
		
		public function MoveAction(unit:Unit, from:Point, to:Point)
		{
			_unit = unit;
			_from = {
				x: from.x,
				y: from.y
			};
			_to = {
				x: to.x,
				y: to.y
			};
		}
		
		public function get object():Object
		{
			return {
				unit: _unit.id,
				from: _from,
				to: _to
			}
		}
		
		public function get type():String
		{
			return _type;
		}
	}
}
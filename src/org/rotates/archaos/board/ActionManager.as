package org.rotates.archaos.board
{
	import flash.utils.ByteArray;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.net.NetManager;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import org.rotates.archaos.board.actions.LocalAction;

	public class ActionManager extends EventDispatcher
	{
		private var _actions:Vector.<LocalAction> = new Vector.<LocalAction>();
		private var _lastAction:uint = 0;
		private var _lastUpdate:uint = 0;
		private var _currentAction:uint = 0;
		private var _board:Board;
		private var _playingActions:Boolean = false;
		
		public function ActionManager(board:Board)
		{
			_board = board;
			Game.NET.client.addEventListener(Event.CHANGE, onData);
		}
		
		public function tick():void
		{
			if (Game.getActiveBoard() && Game.getActiveBoard().id === _board.id && _currentAction < _lastAction && _playingActions == false) {
				_playingActions = true;
				currentAction++;
				var action:LocalAction = getAction(_currentAction);
				trace("Performing action:", action.type);
				action.perform(function():void {
					_playingActions = false;
					Starling.juggler.delayCall(tick, 0.5);
				});
				return;
			}
			_playingActions = false;
		}
		
		private function onData(e:Event, data:Object):void
		{
			if (data.hasOwnProperty("game") && data.game === _board.id) {
				if (data.hasOwnProperty("id") && data.hasOwnProperty("type") && data.hasOwnProperty("data")) {
					addAction(data.id, data.type, data.data, true);
				}
			}
		}
		
		public function clear():void
		{
			_actions = new Vector.<LocalAction>();
			_lastAction = 0;
			_lastUpdate = 0;
			_currentAction = 0;
		}
		
		private function getAction(id:uint):LocalAction
		{
			for (var i:uint = 0; i < _actions.length; i++) {
				if (_actions[i].id == id) {
					return _actions[i];
				}
			}
			return null;
		}
		
		// A generic object comparison function - http://www.actionscript.org/forums/showthread.php3?t=26760
		private function compareActions(obj1:Object, obj2:Object):Boolean
		{
			var buffer1:ByteArray = new ByteArray();
			buffer1.writeObject(obj1);
			var buffer2:ByteArray = new ByteArray();
			buffer2.writeObject(obj2);
			var size:uint = buffer1.length;
			if (buffer1.length == buffer2.length) {
				buffer1.position = 0;
				buffer2.position = 0;
				while (buffer1.position < size) {
					var v1:int = buffer1.readByte();
					if (v1 != buffer2.readByte()) {
						return false;
					}
				}    
				return true;                        
			}
			return false;
		}
		
		public function addAction(id:uint, type:String, data:Object, verified:Boolean = false):void
		{
			const expectedAction:uint = _lastAction + 1;
			var action:LocalAction;
			
			// Is the action concurrent with previous actions? If so, create a new action and if appropriate, mark it as verified.
			if (id === expectedAction) {
				_lastAction = id;
				action = new LocalAction(_lastAction, type, data, _board);
				_actions.push(action);
				if (verified === true) {
					action.verify();
				}
				else {
					Game.NET.client.sendAction(_board.id, type, data, function(data:Object):void {
						if (data.response.hasOwnProperty("success")) {
							var action:LocalAction = getAction(data.response.data.actionId);
							if (action != null) {
								action.verify();
								_lastAction = data.response.data.actionId;
							}
						}
					});
				}
			}
			// If the action is not concurrent, either verify existing actions, throw a desync or attempt to queue the actions from the expected ID.
			else {
				if (id < expectedAction) {
					// Do we have an existing unverified action? If so, and if this action is verified, then we can verify the existing action. If not, a desync has occurred.
					action = getAction(id);
					if (action != null && verified === true) {
						if (action.verified === true) {
							return;
						}
						// trace(JSON.stringify(action.data), JSON.stringify(data));
						if (compareActions(action.data, data) === true) {
							action.verify();
							return;
						}
						trace("*** Actions not synced, doing update");
						_board.update(null, true);
					}
				}
				else if (id > expectedAction) {
					trace("*** Loading previous actions");
					loadActions(expectedAction);
				}
			}
			tick();
		}
		
		private function loadActions(since:uint):void
		{
			Game.NET.client.getGameActions(_board.id, since, function(data:Object):void {
				if (data.response.hasOwnProperty("success")) {
					for (var a:uint; a < data.response.success.length; a++) {
						addAction(data.response.success[a].id, data.response.success[a].type, data.response.success[a].data, true); 
					}
				}
			});
		}
		
		public function get lastAction():uint
		{
			return _lastAction;
		}
		
		public function set lastAction(value:uint):void
		{
			_lastAction = value;
		}
		
		public function get currentAction():uint 
		{
			return _currentAction;
		}
		
		public function set currentAction(value:uint):void
		{
			_currentAction = value;	
		}
	}
}
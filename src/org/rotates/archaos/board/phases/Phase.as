package org.rotates.archaos.board.phases
{
	import org.rotates.archaos.board.Unit;
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.Cursor;
	import flash.geom.Point;

	public class Phase
	{
		
		// phase states
		public static const PHASE_STOPPED:uint = 0;
		public static const PHASE_SPELL_SELECT:uint = 1;
		public static const PHASE_CAST:uint = 2;
		public static const PHASE_MOVE:uint = 3;
		public static const PHASE_AI:uint = 4;
		
		public function Phase()
		{
			
		}
	}
}
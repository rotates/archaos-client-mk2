package org.rotates.archaos.board.phases
{
	import flash.geom.Point;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.Cursor;
	import org.rotates.archaos.board.Dice;
	import org.rotates.archaos.board.Gizmos.MoveGizmo;
	import org.rotates.archaos.board.Gizmos.Node;
	import org.rotates.archaos.board.Gizmos.Path;
	import org.rotates.archaos.board.Unit;
	import org.rotates.archaos.board.actions.*;
	
	public class RuleManager
	{		
		private var _rules:Vector.<Rule> = new Vector.<Rule>();
		private var _board:Board;
		

		public function RuleManager(board:Board)
		{
			_board = board;
			
			/*	MOVE PHASE
			- SELECTION STATE
			- Select a unit
			*/
			_rules.push(new Rule(Phase.PHASE_MOVE, Board.STATE_SELECTING_UNIT,
				function(pos:Point, unit:Unit):Boolean {
					if (unit !== null && unit.isOwnedBy(_board.currentPlayer) && (unit.canMove() || (_board.getAdjacent(unit, "attackable").length > 0 && unit.canAttack()) || unit.canRangedAttack())) {
						return true;
					}
					return false;
				},
				function(pos:Point, unit:Unit):void {
					_board.cursor.type = Cursor.SELECT;
				},
				function(pos:Point, unit:Unit):void {
					_board.selectedUnit = unit;
					if (unit) {
						if (unit.canMove()) {
							_board.currentState = Board.STATE_MOVING;
							_board.gizmos["moveGizmo"].generate(unit);
						}
						else if (unit.engaged === true && _board.getAdjacent(unit, "engageable").length > 0 && unit.canAttack()) {
							_board.currentState = Board.STATE_ENGAGED;
						}
						else if (unit.canAttack() && _board.getAdjacent(unit, "live").length > 0) {
							_board.currentState = Board.STATE_ATTACKING;
						}
						else if (unit.canRangedAttack()) {
							_board.currentState = Board.STATE_RANGED_ATTACKING;
						}
						else {
							_board.currentState = Board.STATE_SELECTING_UNIT;
						}
					}
				},
				function(pos:Point, unit:Unit):void {
					if (unit === null) {
						_board.cursor.type = Cursor.IDLE;
					}
					else {
						_board.cursor.type = Cursor.INVALID;
					}
				},
				null
			));
			
			/*	MOVE PHASE
			- MOVING STATE
			- Move a unit
			*/
			
			_rules.push(new Rule(Phase.PHASE_MOVE, Board.STATE_MOVING,
				function(pos:Point, unit:Unit):Boolean {
					if (_board.selectedUnit !== null) {
						if ((unit !== _board.selectedUnit) && (!_board.selectedUnit.canFly() && _board.gizmos["moveGizmo"].getPathTo(pos)) || (_board.selectedUnit.canFly() && (_board.gizmos["moveGizmo"].getNode(pos))) || (unit && unit.attackableBy(_board.selectedUnit) && Board.getDistance(unit.position, _board.selectedUnit.position) <= _board.selectedUnit.properties.mov + 0.5)) {
							return true;
						}
					}
					return false;
				},
				function(pos:Point, unit:Unit):void {
					var node:Node;
					if (_board.selectedUnit.canFly()) {
						node = _board.gizmos["moveGizmo"].getNode(pos);
						if (node) {
							if (unit) {
								if (unit.isOwnedBy(_board.selectedUnit.owner))  {
									_board.cursor.type = Cursor.WARNING;
								}
								else {
									_board.cursor.type = Cursor.ATTACK;	
								}
							}
							else {
								if (node.warning === true) {
									_board.cursor.type = Cursor.WARNING;
								}
								else {
									_board.cursor.type = Cursor.FLY;
								}
							}
						}
					}
					else {
						_board.gizmos["moveGizmo"].showPath(pos);
						node = _board.gizmos["moveGizmo"].getNode(pos);
						if (node) {
							if (unit) {
								if (unit.isOwnedBy(_board.selectedUnit.owner))  {
									_board.cursor.type = Cursor.WARNING;
								}
								else {
									_board.cursor.type = Cursor.ATTACK;	
								}
							}
							else {
								if (node.warning === true) {
									_board.cursor.type = Cursor.WARNING;
								}
								else {
									_board.cursor.type = Cursor.MOVE;
								}
							}
						}
					}
				},
				function(pos:Point, unit:Unit):void {
					if (_board.selectedUnit.canFly()) {
						var destination:Node = _board.gizmos["moveGizmo"].getNode(pos), success:Boolean;
						if (destination && _board.selectedUnit) {
							if (unit && unit.attackableBy(_board.selectedUnit)) {
								success = Dice.rollAgainst(_board, _board.selectedUnit.properties.com, unit.properties.def);
								_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.ATTACKED, new AttackedAction(_board.selectedUnit, unit, success).object);
								/*
								if (success === true) {
									_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.KILLED, new KilledUnitAction(unit).object);
									if (_board.selectedUnit.properties.mov > 0 && unit.rider === null) {
										_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.MOVED, new MoveFlyAction(_board.selectedUnit, _board.selectedUnit.position, destination.pos).object);
									}
								}
								*/
							}
							else {
								_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.MOVED, new MoveFlyAction(_board.selectedUnit, _board.selectedUnit.position, destination.pos).object);
								/*
								if (destination.warning === true) {
									_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.ENGAGED, new EngagedAction([_board.selectedUnit.id]).object);
								}
								*/
							}
							if (_board.selectedUnit.canRangedAttack()) {
								_board.currentState = Board.STATE_RANGED_ATTACKING;	
							}
							else {
								_board.currentState = Board.STATE_SELECTING_UNIT;
							}
							_board.gizmos["moveGizmo"].reset();
						}
					}
					else {
						var path:Path = _board.gizmos["moveGizmo"].getPathTo(pos);
						if (path && _board.selectedUnit) {
							if (unit && unit.attackableBy(_board.selectedUnit)) {
								_board.selectedUnit.path = path.toPoints();
								_board.selectedUnit.path.pop();
								if (_board.selectedUnit.path.length > 1) {
									_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.MOVED, new MovePathAction(_board.selectedUnit).object);
									// _board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.ENGAGED, new EngagedAction([_board.selectedUnit.id]).object);
								}
								else {
									success = Dice.rollAgainst(_board, _board.selectedUnit.properties.com, unit.properties.def);
									_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.ATTACKED, new AttackedAction(_board.selectedUnit, unit, success).object);
									/*
									if (success === true) {
										_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.KILLED, new KilledUnitAction(unit).object);
										if (_board.selectedUnit.properties.mov > 0 && unit.rider === null) {
											_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.MOVED, new MoveAction(_board.selectedUnit, _board.selectedUnit.position, destination.pos).object);
										}
									}
									*/
								}
							}
							else {
								_board.selectedUnit.path = path.toPoints();
								_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.MOVED, new MovePathAction(_board.selectedUnit).object);
								/*
								if (path.warning === true) {
									_board.actionManager.addAction(_board.actionManager.lastAction+1, LocalAction.ENGAGED, new EngagedAction([_board.selectedUnit.id]).object);
								}
								*/
							}
							if (_board.selectedUnit.canRangedAttack()) {
								_board.currentState = Board.STATE_RANGED_ATTACKING;	
							}
							else {
								_board.currentState = Board.STATE_SELECTING_UNIT;
							}
							_board.gizmos["moveGizmo"].reset();
						}
					}
				},
				function(pos:Point, unit:Unit):void {
					if (!_board.selectedUnit.canFly()) {
						_board.gizmos["moveGizmo"].hidePath();
					}
					_board.cursor.type = Cursor.INVALID;
				}				
			));
		}
		
		public function selectHandler(pos:Point, confirm:Boolean = false):void
		{
			var unit:Unit = _board.getUnitAt(pos.x, pos.y), ruleList:Vector.<Rule> = new Vector.<Rule>(), r:uint;
			
			_board.renderers[1].hideInfo();
			if (unit) {
				_board.renderers[1].showInfo(unit);
			}
			
			for (r = 0; r < _rules.length; r++) {
				if (_rules[r].phase === _board.currentPhase && _rules[r].state === _board.currentState) {
					ruleList.push(_rules[r]);
				}
			}
			
			ruleList = ruleList.sort(function(r1:Rule, r2:Rule):Number { return r1.priority < r2.priority ? -1 : 1 });
			
			for (r = 0; r < ruleList.length; r++) {
				if (confirm === true) {
					ruleList[r].confirm.call(null, pos, unit);
				}
				else {
					ruleList[r].intent.call(null, pos, unit);
				}
			}
		}
	}
}
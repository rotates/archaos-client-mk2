package org.rotates.archaos.board.phases
{
	import flash.geom.Point;
	
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.Unit;

	public class Rule
	{
		private var _phase:uint;
		private var _state:String;
		private var _test:Function;
		private var _successIntent:Function;
		private var _success:Function;
		private var _failureIntent:Function;
		private var _failure:Function;
		private var _priority:uint;
		
		public function Rule(phase:uint, state:String, test:Function, successIntent:Function = null, success:Function = null, failureIntent:Function = null, failure:Function = null, priority:uint = 1)
		{
			_phase = phase;
			_state = state;
			_test = test;
			_successIntent = successIntent;
			_success = success;
			_failureIntent = failureIntent;
			_failure = failure;
			_priority = priority;
		}
		
		public function intent(pos:Point, unit:Unit = null):void
		{
			if (_test(pos, unit) === true) {
				if (_successIntent !== null) {
					_successIntent.call(null, pos, unit);
					return;
				}
			}
			if (_failureIntent !== null) {
				_failureIntent.call(null, pos, unit);
				return;
			}
		}
		
		public function confirm(pos:Point, unit:Unit = null):void
		{
			if (_test(pos, unit) === true) {
				if (_success !== null) {
					_success.call(null, pos, unit);
					return;
				}
			}
			if (_failure !== null) {
				_failure.call(null, pos, unit);
				return;
			}
		}
		
		public function get phase():uint
		{
			return _phase;
		}
		
		public function get state():String
		{
			return _state;
		}
		
		public function get priority():uint 
		{
			return _priority;
		}
	}
}
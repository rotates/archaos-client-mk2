package org.rotates.archaos.board.phases
{
	import flash.geom.Point;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.Cursor;
	import org.rotates.archaos.board.Unit;
	import org.rotates.archaos.board.actions.*;
	import org.rotates.archaos.board.phases.Intent;

	public class MovePhase extends Phase
	{		
		public function MovePhase()
		{
		}
		
		public static function execute(board:Board, pos:Point):Intent 
		{
			if (board.currentPlayer.username === Game.username) {
				switch (board.currentState) {
					case Board.STATE_SELECTING_UNIT:
						return selectingUnit(board, pos);
						break;
					case Board.STATE_MOVING:
						return movingUnit(board, pos);
						break;
					case Board.STATE_ATTACKING:
						return attackingUnit(board, pos);
						break;
					case Board.STATE_RANGED_ATTACKING:
						break;
					case Board.STATE_ENGAGED:
						break;
					case Board.STATE_DISMOUNTING:
						break;
				}
			}
			return null;
		}
		
		private static function attackingUnit(board:Board, pos:Point):Intent
		{
			var unit:Unit = board.getUnitAt(pos.x, pos.y), dist:Number;
			return null;
		}
		
		private static function movingUnit(board:Board, pos:Point):Intent
		{
			var unit:Unit = board.getUnitAt(pos.x, pos.y),
				dist:Number = Board.getDistance(board.selectedUnit.position, pos),
				originalPos:Point = board.selectedUnit.position.clone();
			
			if (unit === null) {
				if (board.selectedUnit.hasStatus("flying")
					&& dist <= board.selectedUnit.properties.mov)
				{
					return new Intent("Fly " + unit.id + " to " + pos.toString(), pos, function():void {
						board.selectedUnit.move(pos.x, pos.y, function():void {
							if (board.selectedUnit == null) {
								return;
							}
							board.selectedUnit.moved = true;
							board.actionManager.addAction(board.actionManager.lastAction+1, LocalAction.MOVED, new MoveFlyAction(board.selectedUnit, originalPos, pos.clone()).object);
							if (board.getAdjacent(board.selectedUnit))
							{
								board.selectedUnit.engaged = true;
								board.actionManager.addAction(board.actionManager.lastAction+1, LocalAction.ENGAGED, new EngagedAction([board.selectedUnit.id]).object);
								board.currentState = Board.STATE_ENGAGED;
							}
							else {
								board.selectedUnit.attacked = true;
								if (board.selectedUnit.hasRangedCombat()
									&& board.selectedUnit.rangedAttacked == false)
								{
									board.currentState = Board.STATE_RANGED_ATTACKING;
								}
								else {
									board.currentState = Board.STATE_SELECTING_UNIT;
								}
							}
						});
					});
				}
				else
				{
					if (dist <= 1.5)
					{
						return new Intent("Move " + board.selectedUnit.id + " to " + pos.toString(), pos, function():void {
							board.selectedUnit.addStep(pos, dist);
							board.selectedUnit.move(pos.x, pos.y, function():void {
								if (board.selectedUnit == null) {
									return;
								}
								if (board.getAdjacent(board.selectedUnit))
								{
									board.selectedUnit.moved = true;
									board.selectedUnit.engaged = true;
									board.actionManager.addAction(board.actionManager.lastAction+1, LocalAction.MOVED, new MovePathAction(board.selectedUnit).object);
									board.selectedUnit.resetSteps();
									board.actionManager.addAction(board.actionManager.lastAction+1, LocalAction.ENGAGED, new EngagedAction([board.selectedUnit.id]).object);
									board.currentState = Board.STATE_ENGAGED;
									return;
								}
								else if (board.selectedUnit.movementAllowance == 0)
								{
									board.actionManager.addAction(board.actionManager.lastAction+1, LocalAction.MOVED, new MovePathAction(board.selectedUnit).object);
									board.selectedUnit.resetSteps();
									board.selectedUnit.moved = true;
									board.selectedUnit.attacked = true;
									if (board.selectedUnit.hasRangedCombat() && board.selectedUnit.rangedAttacked == false) {
										board.currentState = Board.STATE_RANGED_ATTACKING;
									}
									else {
										board.currentState = Board.STATE_SELECTING_UNIT;
									}
								}
							});
						});
					}
				}
			}
			else {
				if (unit.id === board.selectedUnit.id) {
					board.cursor.type = Cursor.INVALID;
					return null;
				}
			}
			board.cursor.type = Cursor.INVALID;
			return null;
		}
		
		/*
		private static function movingUnit(board:Board, pos:Point):Intent
		{
			var unit:Unit = board.getUnitAt(pos.x, pos.y), dist:Number, originalPos:Point;
			// Is a unit selected?  Can it move? Does the current player own the unit?
			if (board.selectedUnit != null
				&& board.selectedUnit.properties.mov > 0
				&& board.selectedUnit.moved == false
				&& board.selectedUnit.engaged == false
				&& board.selectedUnit.owner.username == board.currentPlayer.username)
			{
				originalPos = board.selectedUnit.position.clone();
				dist = Board.getDistance(board.selectedUnit.position, pos);
				// Is the target position a unit or an empty tile?
				if (unit != null)
				{
					if (unit == board.selectedUnit) {
						return null;
					}
					// Is the target unit a mount? Can it be mounted by the currently selected unit? Is the unit close enough to mount?
					if (unit.hasStatus("mount")
						&& board.selectedUnit.isWizard()
						&& board.selectedUnit.isRider == false
						&& unit.owner.username == board.currentPlayer.username
						&& unit.rider == null
						&& dist <= 1.5)
					{
						if (board.selectedUnit.path.length > 0)
						{
							board.actionManager.addAction(board.actionManager.lastAction+1, Action.MOVED, new MovePathAction(board.selectedUnit).object);
						}
						board.actionManager.addAction(board.actionManager.lastAction+1, Action.MOUNTED, new MountAction(board.selectedUnit, unit).object);
						unit.mountRider(board.selectedUnit);
						board.currentState = Board.STATE_SELECTING_UNIT;
					}
					else
					{
						if (unit.attackableBy(board.selectedUnit))
						{
							if ((board.selectedUnit.hasStatus("flying")	&& dist <= board.selectedUnit.properties.mov)
								|| dist <= 1.5)
							{
								board.currentState = Board.STATE_ATTACKING;
								attackingUnit(board, pos);
							}							
						}
					}
				}
				else
				{
					if (board.selectedUnit.hasStatus("flying")
						&& dist <= board.selectedUnit.properties.mov)
					{
						board.selectedUnit.move(pos.x, pos.y, function():void {
							if (board.selectedUnit == null) {
								return;
							}
							board.selectedUnit.moved = true;
							board.actionManager.addAction(board.actionManager.lastAction+1, Action.MOVED, new MoveFlyAction(board.selectedUnit, originalPos, pos.clone()).object);
							if (board.getAdjacent(board.selectedUnit))
							{
								board.selectedUnit.engaged = true;
								board.actionManager.addAction(board.actionManager.lastAction+1, Action.ENGAGED, new EngagedAction([board.selectedUnit.id]).object);
								board.currentState = Board.STATE_ENGAGED;
							}
							else {
								board.selectedUnit.attacked = true;
								if (board.selectedUnit.hasRangedCombat()
									&& board.selectedUnit.rangedAttacked == false)
								{
									board.currentState = Board.STATE_RANGED_ATTACKING;
								}
								else {
									board.currentState = Board.STATE_SELECTING_UNIT;
								}
							}
						});
					}
					else
					{
						if (dist <= 1.5)
						{
							board.selectedUnit.addStep(pos, dist);
							board.selectedUnit.move(pos.x, pos.y, function():void {
								if (board.selectedUnit == null) {
									return;
								}
								if (board.getAdjacent(board.selectedUnit))
								{
									board.selectedUnit.moved = true;
									board.selectedUnit.engaged = true;
									board.actionManager.addAction(board.actionManager.lastAction+1, Action.MOVED, new MovePathAction(board.selectedUnit).object);
									board.selectedUnit.resetSteps();
									board.actionManager.addAction(board.actionManager.lastAction+1, Action.ENGAGED, new EngagedAction([board.selectedUnit.id]).object);
									board.currentState = Board.STATE_ENGAGED;
									return;
								}
								else if (board.selectedUnit.movementAllowance == 0)
								{
									board.actionManager.addAction(board.actionManager.lastAction+1, Action.MOVED, new MovePathAction(board.selectedUnit).object);
									board.selectedUnit.resetSteps();
									board.selectedUnit.moved = true;
									board.selectedUnit.attacked = true;
									if (board.selectedUnit.hasRangedCombat() && board.selectedUnit.rangedAttacked == false) {
										board.currentState = Board.STATE_RANGED_ATTACKING;
									}
									else {
										board.currentState = Board.STATE_SELECTING_UNIT;
									}
								}
							});
						}
					}
				}
			}
			return null;
		}
		*/
		
		private static function selectingUnit(board:Board, pos:Point):Intent {
			var unit:Unit = board.getUnitAt(pos.x, pos.y);
			if (unit === null)
			{
				board.cursor.type = Cursor.IDLE;
				return null;
			}
			if (board.selectedUnit !== null
				|| unit.owner.username !== board.currentPlayer.username
				|| (unit.properties.com === 0 && unit.properties.mov === 0)
				|| (unit.attacked === false && !board.getAdjacent(board.selectedUnit, "live")))
			{
				board.cursor.type = Cursor.INVALID;
				return null;
			}
			board.cursor.type = Cursor.SELECT;
			return new Intent("Select " + unit.id, pos, function():void {
				board.selectedUnit = unit;
				if (unit.moved)
				{
					if (!unit.attacked)
					{
						if (unit.engaged == true)
						{
							board.currentState = Board.STATE_ENGAGED;
						}
						else {
							if (board.getAdjacent(board.selectedUnit, "live")) {
								board.currentState = Board.STATE_ATTACKING;
							}
							else {
								board.currentState = Board.STATE_SELECTING_UNIT;
							}
						}
					}
					else if (unit.hasRangedCombat()
						&& unit.rangedAttacked == false)
					{
						board.currentState = Board.STATE_RANGED_ATTACKING;
					}
					else {
						board.currentState = Board.STATE_SELECTING_UNIT;
					}
				}
				else if (board.selectedUnit.properties.mov < 1)
				{
					if (board.selectedUnit.properties.com > 0
						|| board.getAdjacent(board.selectedUnit, "live"))
					{
						board.currentState = Board.STATE_ATTACKING;
					}
					else if (board.selectedUnit.hasRangedCombat())
					{
						board.currentState = Board.STATE_RANGED_ATTACKING;
					}
					else {
						board.currentState = Board.STATE_SELECTING_UNIT;
					}
				}
				else
				{
					board.currentState = Board.STATE_MOVING;
				}
			});
		}
	}
}
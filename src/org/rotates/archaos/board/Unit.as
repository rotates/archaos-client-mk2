package org.rotates.archaos.board
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Quad;
	
	import flash.geom.Point;
	
	import org.rotates.archaos.board.renderers.BoardRenderer;
	import org.rotates.archaos.board.renderers.IsoUnit;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;

	public class Unit extends EventDispatcher
	{	
		private var _id:String;
		private var _name:String;
		private var _owner:Player;
		private var _type:String;
		private var _position:Point = new Point(-1,-1);
		private var _properties:Object;
		private var _status:Vector.<String> = new Vector.<String>();
		
		private var _moved:Boolean;
		private var _attacked:Boolean;
		private var _rangedAttacked:Boolean;
		private var _engaged:Boolean;
		private var _dead:Boolean;
		private var _illusion:Boolean = false;
		private var _renderers:Vector.<IsoUnit> = new Vector.<IsoUnit>();
		private var _rider:Unit;
		private var _isRider:Boolean;
		
		private var _path:Vector.<Point> = new Vector.<Point>();
		private var _steps:Number = 0;
		
		public function Unit(data:Object)
		{
			if (data.hasOwnProperty("id")
				&& data.hasOwnProperty("owner")
				&& data.hasOwnProperty("type")
				&& data.hasOwnProperty("position")
				&& data.hasOwnProperty("properties")
				&& data.hasOwnProperty("moved")
				&& data.hasOwnProperty("attacked")
				&& data.hasOwnProperty("rangedAttacked")
				&& data.hasOwnProperty("engaged")
				&& data.hasOwnProperty("dead"))
			{
				this._id = data.id;
				this._owner = data.owner;
				if (data.position.hasOwnProperty("x") && data.position.hasOwnProperty("y")) {
					this.setPosition(data.position.x, data.position.y);
				}
				else {
					throw new Error("Invalid unit position");
				}
				this._type = data.type;
				this._properties = data.properties;
				this._moved = data.moved;
				this._attacked = data.attacked;
				this._rangedAttacked = data.rangedAttacked;
				this._engaged = data.engaged;
				this._dead = data.dead;
			}
			else {
				throw new Error("Missing required unit data");
			}
			
			if (data.hasOwnProperty("status") && data.status.length > 0) {
				for (var s:uint = 0; s < data.status.length; s++) {
					this._status.push(data.status[s]);
				}
			}
			
			this._properties = data.properties;
			
			if (data.hasOwnProperty("illusion")) {
				this._illusion = data.illusion;
			}
		}
		
		public function addStep(pos:Point, distance:Number):Number
		{
			if (distance <= 1.5) {
				if (_path.length < 1) {
					_path.push(_position.clone());
				}
				_path.push(pos.clone());
				_steps += distance;
				return _steps;
			}
			return 0;
		}
		
		public function get movementAllowance():uint
		{
			if (_moved == true) {
				return 0;
			}
			return Math.max(0, _properties.mov-_steps);
		}
		
		public function resetSteps():void {
			_path.length = 0;
			_steps = 0;
		}
		
		public function reset():void
		{
			_moved = false;
			_engaged = false;
			_attacked = false;
			_rangedAttacked = false;
			resetSteps();
		}
		
		public function get path():Vector.<Point>
		{
			return _path;
		}
		
		public function set path(value:Vector.<Point>):void
		{
			_path = value;
		}
		
		public function isOwnedBy(player:Player):Boolean
		{
			if (_owner.username === player.username) {
				return true;
			}
			return false;
		}
		
		public function isWizard():Boolean
		{
			if (_type === "Wizard") {
				return true;
			}
			return false;
		}
		
		public function set isRider(value:Boolean):void
		{
			if (isWizard() == true) {
				_isRider = value;
			}
			else {
				throw new Error("Only wizards may mount");
			}
		}
		
		public function get isRider():Boolean
		{
			return _isRider || false;
		}
		
		public function get rider():Unit
		{
			return _rider;
		}
		
		public function set rider(value:Unit):void
		{
			_rider = value;
			_rider.renderersAsArray.forEach(function(r:IsoUnit, i:int, a:*):void {
				r.alpha = 0;
				r.visible = false;
			});
		}
		
		public function mountRider(rider:Unit, callback:Function = null):void
		{
			if (hasStatus("mount")) {
				if (_rider == null) {
					_rider = rider;
					_rider.isRider = true;
					if (_renderers.length > 0) {
						TweenMax.allTo(_rider.renderersAsArray, 1, { x: _position.x, y: _position.y, ease: Quad.easeInOut, alpha: 0, visible: false, onComplete: function():void {
							if (callback != null) {
								callback.call();
							}
						}});
					}
				}
				else {
					throw new Error("Cannot mount a unit that is already mounted");
				}
			}
			else {
				throw new Error("This unit cannot be mounted");
			}
		}
		
		public function canMove():Boolean
		{
			if (this._properties.mov > 0 && this._moved === false && this._engaged === false) {
				return true;
			}
			return false;
		}
		
		public function canAttack():Boolean
		{
			if (this._properties.com > 0 && this._attacked === false) {
				return true;
			}
			return false;		
		}
		
		public function canRangedAttack():Boolean
		{
			if (this._properties.rcm > 0 && this._properties.rng > 0 && this._rangedAttacked === false) {
				return true;
			}
			return false;
		}
		
		public function canFly():Boolean
		{
			if (hasStatus("flying")) {
				return true;
			}
			return false;
		}
		
		public function attackableBy(unit:Unit):Boolean
		{
			if (unit.id == this._id || hasStatus("invuln")) {
				return false;
			}
			if (hasStatus("undead")) {
				if (unit.hasStatus("undead") || unit.hasStatus("attackUndead")) {
					return true;
				}
			}
			return true;
		}
		
		public function engageableBy(unit:Unit):Boolean
		{
			if (unit != this && this._properties.mnv > 0 && !this.isOwnedBy(unit.owner)) {
				return true;
			}
			return false;
		}
		
		public function addRenderer(renderer:IsoUnit):void
		{
			_renderers.push(renderer);
			setPosition(_position.x, _position.y);
		}
		
		public function removeRenderer(renderer:IsoUnit):void 
		{
			_renderers.splice(_renderers.indexOf(renderer), 1);
		}
		
		public function get renderers():Vector.<IsoUnit>
		{
			return _renderers;
		}

		public function get illusion():Boolean
		{
			return _illusion;
		}

		public function set illusion(value:Boolean):void
		{
			_illusion = value;
		}

		public function get dead():Boolean
		{
			return _dead;
		}

		public function set dead(value:Boolean):void
		{
			_dead = value;
			
			_renderers.forEach(function(r:IsoUnit, i:int, a:*):void {
				r.dead = true;					
			});
		}

		public function get engaged():Boolean
		{
			return _engaged;
		}

		public function set engaged(value:Boolean):void
		{
			_engaged = value;
		}

		public function get rangedAttacked():Boolean
		{
			return _rangedAttacked;
		}

		public function set rangedAttacked(value:Boolean):void
		{
			_rangedAttacked = value;
		}

		public function get attacked():Boolean
		{
			return _attacked;
		}

		public function set attacked(value:Boolean):void
		{
			_attacked = value;
		}

		public function get moved():Boolean
		{
			return _moved;
		}

		public function set moved(value:Boolean):void
		{
			_moved = value;
		}

		public function get properties():Object
		{
			return _properties;
		}

		public function get position():Point
		{
			return _position;
		}
		
		private function get renderersAsArray():Array
		{
			if (_renderers.length > 0) {
				var output:Array = new Array();
				for (var i:uint = 0; i < _renderers.length; i++) {
					output.push(_renderers[i]);
				}
				return output;
			}
			return null;
		}
				
		public function move(x:uint, y:uint, callback:Function = null, time:Number = 0.5):void
		{
			if (_position.x == x && _position.y == y) {
				callback.call();
				return;
			}
			var dir:String = null;
			if ((_position.x - _position.y) < (x-y)) {
				dir = IsoUnit.DIRECTION_RIGHT;
			}
			else if ((_position.x - _position.y) > (x-y)) {
				dir = IsoUnit.DIRECTION_LEFT;
			}
			_position.x = x;
			_position.y = y;
			if (_rider != null) {
				_rider.setPosition(_position.x,_position.y);
			}
			if (_renderers.length > 0) {
				if (dir != null) {
					_renderers.forEach(function(r:IsoUnit, i:int, a:*):void {
						r.direction = dir;					
					});
				}
				TweenMax.allTo(renderersAsArray, time * 0.5, { z: "5", yoyo: true, repeat: 1});
				TweenMax.allTo(renderersAsArray, time, { x: _position.x, y: _position.y}, 0, function():void {
					if (callback != null) {
						callback.call();
					}
				});
			}
			else {
				if (callback != null) {
					callback.call();
				}
			}
		}
		
		public function movePath(path:*, callback:Function = null):void
		{
			if (_renderers.length > 0) {
				var p:Object;
				var path:Array = path;
				path.shift();
				var step:Function = function():void {
					p = path.shift();					
					if (p) {
						move(p.x, p.y, function():void {
							if (path.length > 0) {
								step();
							}
							else {
								if (callback !== null) {
									callback.call();
								}
							}
						});
					}
				};
				step();
			}
			else {
				_position.x = path[path.length-1].x;
				_position.y = path[path.length-1].y;
				if (callback != null) {
					callback.call();
				}
			}
		}

		public function setPosition(x:uint, y:uint):void
		{
			_position.x = x;
			_position.y = y;
			if (_rider != null) {
				_rider.setPosition(_position.x,_position.y);
			}
			if (_renderers.length > 0) {
				_renderers.forEach(function(r:IsoUnit, i:int, a:*):void {
					r.x = _position.x;
					r.y = _position.y;					
				});
			}
		}

		public function get owner():Player
		{
			return _owner;
		}

		public function set owner(value:Player):void
		{
			_owner = value;
		}

		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			_name = value;
		}

		public function get id():String
		{
			return _id;
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function addStatus(value:String):void
		{
			if (_status.indexOf(value) === -1) {
				_status.push(value);
			}
		}
		
		public function removeStatus(value:String):void
		{
			if (_status.indexOf(value) >= 0) {
				_status.splice(_status.indexOf(value), 1);
			}
		}
		
		public function hasStatus(value:String):Boolean
		{
			if (_status.indexOf(value) !== -1) {
				return true;
			}
			return false;		
		}
	}
}
package org.rotates.archaos.board.Gizmos
{
	import com.greensock.TweenMax;
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.Cursor;
	import org.rotates.archaos.board.Unit;
	import org.rotates.archaos.board.renderers.BoardRenderer;
	import org.rotates.archaos.board.renderers.IsoImage;
	
	import starling.display.QuadBatch;

	public class MoveGizmo
	{
		public static const CLEAR:String = "clear";
		public static const WARNING:String = "warning";
		public static const OBSTRUCTED:String = "obstructed"; 
		
		private var _board:Board;
		private var _unit:Unit = null;
		private var _validNodes:Vector.<Node> = new Vector.<Node>();
		private var _paths:Object = new Object();
		private var _rect:Rectangle = new Rectangle();
		
		public function MoveGizmo(board:Board)
		{
			_board = board;
		}
		
		public function generate(unit:Unit):void
		{
			_validNodes = new Vector.<Node>();
			_paths = new Object();
			_unit = unit;
			
			var node:Node;
			
			_rect.left = Math.max(0, unit.position.x - unit.properties.mov);
			_rect.right = Math.min(_board.boardWidth-1, unit.position.x + unit.properties.mov);
			_rect.top = Math.max(0, unit.position.y - unit.properties.mov);
			_rect.bottom  = Math.min(_board.boardHeight-1, unit.position.y + unit.properties.mov);

		
			for (var xx:uint = _rect.left; xx <= _rect.right; xx++) {
				for (var yy:uint = _rect.top; yy <= _rect.bottom; yy++) {
					node = new Node(xx,yy);
					if ((_unit.canFly() && Board.getDistance(node.pos, _unit.position) > _unit.properties.mov + 0.5)) {
						node.traversable = false;	
					}
					if (_board.getUnitAt(xx,yy)) {
						node.warning = true;
					}
					if (_unit.canFly()) {
						node.flying = true;
					}
					_validNodes.push(node);
				}
			}
			
			var enemy:Unit, ePt:Point = new Point(0,0), wPt:Point = new Point(0,0);
			
			for (var e:uint = 0; e < _validNodes.length; e++) {
				enemy = _board.getUnitAt(_validNodes[e].x,_validNodes[e].y);
				if (enemy && enemy.engageableBy(_unit)) {
					ePt = enemy.position.clone();
					for (var ex:int = ePt.x-1; ex < ePt.x+2; ex++) {
						for (var ey:int = ePt.y-1; ey < ePt.y+2; ey++) {
							wPt.setTo(ex,ey);
							node = getNode(wPt);
							if (node) {
								node.warning = true;
							}
						}
					}
				}
			}
			
			if (_unit.properties.mov > 1) {
				_validNodes = _validNodes.sort(function(n1:Node, n2:Node):Number { return Board.getDistance(_unit.position, n1.pos) < Board.getDistance(_unit.position, n2.pos) ? -1 : 1; });
			}
			
			if (!_unit.canFly()) {
				generatePaths();
			}
			else {
				generateRange();
			}
		}
		
		public function reset():void
		{
			_unit = null;
			_board.renderers[1].gizmos.reset();
		}
		
		private function generatePaths(delay:uint = 15, step:uint = 3):void {
			_board.renderers[1].gizmos.reset();
			var path:Path, timer:Timer = new Timer(delay, _validNodes.length / step), i:uint = 0, s:uint = 0;
			timer.addEventListener(TimerEvent.TIMER, toNode);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, finish);
			timer.start();
			function toNode(e:TimerEvent = null):void {
				for (s = i; s < Math.min(i+step, _validNodes.length); s++) {
					if (_validNodes[s] && _validNodes[s].traversable) {
						path = getPathTo(_validNodes[s].pos); 
						if (path) {
							if (path.cost > _unit.properties.mov+1) {
								_validNodes[s].traversable = false;
							}
							else {
								_validNodes[s].path = path;
								var isoImage:IsoImage;
								if (_validNodes[s].warning === true) {
									isoImage = new IsoImage(Game.ASSETS.getTexture("cursors/range-move-warning"), BoardRenderer.TILE_SIZE, BoardRenderer.TILE_SIZE);
								}
								else {
									isoImage = new IsoImage(Game.ASSETS.getTexture("cursors/range-move"), BoardRenderer.TILE_SIZE, BoardRenderer.TILE_SIZE);
								}
								isoImage.x = _validNodes[s].x;
								isoImage.y = _validNodes[s].y;
								isoImage.smoothing = Main.TEXTURE_SMOOTHING;
								_board.renderers[1].gizmos.addImage(isoImage);
							}
						}
					}
				}
				i += step;
			}
			
			function finish(e:TimerEvent = null):void {
				timer.removeEventListener(TimerEvent.TIMER, toNode);
				timer.removeEventListener(TimerEvent.TIMER_COMPLETE, finish);
			}
		}
		
		private function generateRange(delay:uint = 20, step:uint = 3):void {
			_board.renderers[1].gizmos.reset();
			var path:Path, timer:Timer = new Timer(delay, _validNodes.length / step), i:uint = 0, s:uint = 0;
			timer.addEventListener(TimerEvent.TIMER, toNode);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, finish);
			timer.start();
			function toNode(e:TimerEvent = null):void {
				for (s = i; s < Math.min(i+step, _validNodes.length); s++) {
					if (_validNodes[s] && _validNodes[s].isValid()) {
						var isoImage:IsoImage;
						if (_validNodes[s].warning === true) {
							isoImage = new IsoImage(Game.ASSETS.getTexture("cursors/range-move-warning"), BoardRenderer.TILE_SIZE, BoardRenderer.TILE_SIZE);
						}
						else {
							isoImage = new IsoImage(Game.ASSETS.getTexture("cursors/range-move"), BoardRenderer.TILE_SIZE, BoardRenderer.TILE_SIZE);
						}
						isoImage.x = _validNodes[s].x;
						isoImage.y = _validNodes[s].y;
						isoImage.smoothing = Main.TEXTURE_SMOOTHING;
						_board.renderers[1].gizmos.addImage(isoImage);
					}
				}
				i += step;
			}
			
			function finish(e:TimerEvent = null):void {
				timer.removeEventListener(TimerEvent.TIMER, toNode);
				timer.removeEventListener(TimerEvent.TIMER_COMPLETE, finish);
			}
		}
		
		private function drawGizmo():void
		{
			_board.renderers[1].gizmos.reset();
			
			var isoImage:IsoImage;
			
			for (var n:uint = 0; n < _validNodes.length; n++) {
				if (_validNodes[n].isValid()) {
					if (_validNodes[n].warning === true) {
						isoImage = new IsoImage(Game.ASSETS.getTexture("cursors/range-move-warning"), BoardRenderer.TILE_SIZE, BoardRenderer.TILE_SIZE);
					}
					else {
						isoImage = new IsoImage(Game.ASSETS.getTexture("cursors/range-move"), BoardRenderer.TILE_SIZE, BoardRenderer.TILE_SIZE);
					}
					isoImage.x = _validNodes[n].x;
					isoImage.y = _validNodes[n].y;
					isoImage.smoothing = Main.TEXTURE_SMOOTHING;
					_board.renderers[1].gizmos.addImage(isoImage);
				}
			}
		}
		
		private static function getAngle(fromPt:Point, toPt:Point):uint
		{
			var a:int = Math.atan2(toPt.y - fromPt.y, toPt.x - fromPt.x) * (180 / Math.PI);
			a += 22.5;
			a = a < 0 ? a + 360 : a;
			return a / 45;
		}
		
		public function getNode(pt:Point):Node
		{
			for (var n:uint = 0; n < _validNodes.length; n++) {
				if (_validNodes[n].pos.equals(pt) && _validNodes[n].traversable === true) {
					return _validNodes[n];
				}
			}
			return null;
		}
		
		public function getPathTo(pt:Point):Path
		{
			var path:Path, node:Node;
			node = getNode(pt);
			if (!_unit || !node || node.traversable === false) {
				return null;
			}
			if (_paths.hasOwnProperty(pt.x +","+ pt.y)) {
				path = _paths[pt.x + ","+ pt.y];
			}
			else {
				path = findPath(_unit.position, pt);
				_paths[pt.x + ","+ pt.y] = path;
			}
			return path || null;
		}
		
		public function showPath(toPt:Point):void
		{
			var path:Path = getPathTo(toPt), isoImage:IsoImage;
			
			drawGizmo();
			
			if (!toPt.equals(_unit.position)) {
				for (var n:uint = 1; n < path.nodes.length; n++) {
					isoImage = new IsoImage(Game.ASSETS.getTexture("cursors/" + Cursor.getCursorAngle(path.angles[n])), BoardRenderer.TILE_SIZE, BoardRenderer.TILE_SIZE);
					isoImage.x = path.nodes[n].x;
					isoImage.y = path.nodes[n].y;
					isoImage.smoothing = Main.TEXTURE_SMOOTHING;
					_board.renderers[1].gizmos.addImage(isoImage);
				}
			}
		}
		
		public function hidePath():void 
		{
			drawGizmo();
		}
		
		public function findPath(fromPt:Point, toPt:Point):Path 
		{
			var firstNode:Node, destinationNode:Node;
			for (var n:uint = 0; n < _validNodes.length; n++) {
				if (_validNodes[n].pos.equals(fromPt)) {
					firstNode = _validNodes[n];
				}
				if (_validNodes[n].pos.equals(toPt)) {
					destinationNode = _validNodes[n];
				}
			}
			
			if (firstNode === null || destinationNode === null) {
				return null;
			}
			
			var openNodes:Vector.<Node> = new Vector.<Node>();
			var closedNodes:Vector.<Node> = new Vector.<Node>();			
			
			var currentNode:Node = firstNode;
			var testNode:Node;
			
			var l:int;
			var i:int;
			
			var connectedNodes:Vector.<Node>;
			var travelCost:Number = 1.0;
			
			var g:Number;
			var h:Number;
			var f:Number;
			
			currentNode.g = 0;
			currentNode.h = MoveGizmo.diagonalHeuristic(currentNode, destinationNode, travelCost);
			currentNode.f = currentNode.g + currentNode.h;
			
			while (currentNode != destinationNode) {
				
				connectedNodes = findConnectedNodes( currentNode );			
				
				l = connectedNodes.length;
				
				for (i = 0; i < l; ++i) {
					
					testNode = connectedNodes[i];
					
					if (testNode == currentNode || testNode.traversable == false) continue;					
					g = currentNode.g + MoveGizmo.diagonalHeuristic(currentNode, testNode, travelCost);
					h = MoveGizmo.diagonalHeuristic( testNode, destinationNode, travelCost);
					f = g + h;
					
					if ( MoveGizmo.isOpen(testNode, openNodes) || MoveGizmo.isClosed( testNode, closedNodes) )
					{
						if(testNode.f > f)
						{
							testNode.f = f;
							testNode.g = g;
							testNode.h = h;
							testNode.parentNode = currentNode;
						}
					}
					else {
						testNode.f = f;
						testNode.g = g;
						testNode.h = h;
						testNode.parentNode = currentNode;
						openNodes.push(testNode);
					}
					
				}
				closedNodes.push( currentNode );
				
				if (openNodes.length == 0) {
					return null;
				}
				openNodes.sort(function(n1:Node, n2:Node):Number { return n1.f < n2.f ? -1 : 1; });
				currentNode = openNodes.shift() as Node;
			}
			return MoveGizmo.buildPath(destinationNode, firstNode);
		}
		
		public function findConnectedNodes(node:Node):Vector.<Node>
		{
			var output:Vector.<Node> = new Vector.<Node>();
			
			for (var n:uint = 0; n < _validNodes.length; n++) {
				if (node.x < _validNodes[n].x -1 || node.x > _validNodes[n].x + 1) { continue; }
				if (node.y < _validNodes[n].y -1 || node.y > _validNodes[n].y + 1) { continue; }
				if (node === _validNodes[n]) { continue; }
				output.push(_validNodes[n]);
			}
			
			return output;
		}
		
		public static function diagonalHeuristic(node:Node, destinationNode:Node, cost:Number = 1.0, diagonalCost:Number = 1.5, warningCost:Number = 999):Number
		{
			var dx:Number = Math.abs(node.x - destinationNode.x);
			var dy:Number = Math.abs(node.y - destinationNode.y);
			
			var diag:Number = Math.min( dx, dy );
			var straight:Number = dx + dy;
			
			if (node.warning === true) {
				return diagonalCost * diag + cost * (straight - 2 * diag) + warningCost;	
			}
			
			return diagonalCost * diag + cost * (straight - 2 * diag);
		}
		
		public static function buildPath(destinationNode:Node, startNode:Node):Path {			
			var path:Vector.<Node> = new Vector.<Node>();
			var angles:Vector.<uint> = new Vector.<uint>();
			var node:Node = destinationNode;
			var cost:Number = 0;
			path.push(node);
			while (node != startNode) {
				cost += Board.getDistance(node.pos, node.parentNode.pos);
				angles.unshift(getAngle(node.parentNode.pos, node.pos));
				node = node.parentNode;
				path.unshift(node);
			}
			angles.unshift(getAngle(startNode.pos, destinationNode.pos));
			return new Path(path, angles, cost);			
		}
		
		public static function isOpen(node:Node, openNodes:Vector.<Node>):Boolean {
			
			var l:int = openNodes.length;
			for (var i:int = 0; i < l; ++i) {
				if ( openNodes[i] == node ) return true;
			}
			
			return false;			
		}
		
		public static function isClosed(node:Node, closedNodes:Vector.<Node>):Boolean {
			
			var l:int = closedNodes.length;
			for (var i:int = 0; i < l; ++i) {
				if (closedNodes[i] == node ) return true;
			}
			
			return false;
		}
	}
}
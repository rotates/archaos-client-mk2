package org.rotates.archaos.board.Gizmos
{
	import flash.geom.Point;

	public class Path
	{
		private var _nodes:Vector.<Node>;
		private var _angles:Vector.<uint>;
		private var _cost:Number;
		
		public function Path(nodes:Vector.<Node>, angles:Vector.<uint>, cost:Number)
		{
			if (nodes && cost) {
				_nodes = nodes;
				_angles = angles;
				_cost = cost;
			}
		}
		
		public function toPoints():Vector.<Point>
		{
			var output:Vector.<Point> = new Vector.<Point>();
			for (var n:uint = 0; n < _nodes.length; n++) {
				output.push(_nodes[n].pos.clone());
			}
			return output;
		}
		
		public function get cost():Number
		{
			return _cost;	
		}
		
		public function get nodes():Vector.<Node>
		{
			return _nodes;
		}
		
		public function get angles():Vector.<uint>
		{
			return _angles;
		}
		
		public function get warning():Boolean
		{
			return _nodes[_nodes.length-1].warning;
		}
	}
}
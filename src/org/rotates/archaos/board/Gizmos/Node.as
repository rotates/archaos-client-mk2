package org.rotates.archaos.board.Gizmos
{
	import flash.geom.Point;

	public class Node
	{
		private var _pos:Point = new Point(-1,-1);
		public var g:Number;
		public var f:Number;
		public var h:Number;
		public var parentNode:Node;
		public var traversable:Boolean = true;
		public var warning:Boolean = false;
		public var path:Path;
		public var flying:Boolean = false;
		
		public function Node(x:int, y:int)
		{
			_pos.x = x;
			_pos.y = y;
		}
		
		public function get x():int
		{
			return _pos.x;
		}
		
		public function get y():int
		{
			return _pos.y;
		}
		
		public function get pos():Point
		{
			return _pos;
		}
		
		public function isValid():Boolean
		{
			if ((path !== null || flying === true) && traversable === true) {
				return true;
			}
			return false;
		}
	}
}
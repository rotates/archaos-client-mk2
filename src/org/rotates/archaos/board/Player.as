package org.rotates.archaos.board
{
	public class Player
	{
		private var _username:String;
		private var _handle:String;
		private var _defeated:Boolean;
		private var _index:uint;
		private var _wizCode:String;
		
		public function Player(data:Object, index:uint)
		{
			if (data.hasOwnProperty("username") && data.hasOwnProperty("handle")) {
				this._username = data.username;
				this._handle = data.handle;
				this._index = index;
			}
			else {
				throw new Error("Invalid player data");
			}
			if (data.hasOwnProperty("defeated")) {
				this._defeated = data.defeated;
			}
			else {
				this._defeated = false;
			}
			if (data.hasOwnProperty("wizCode")) {
				this._wizCode = data.wizCode;
			}
			else {
				this._wizCode = "0" + this._index + "0" + this._index + "0" + this._index + "0000";
			}
		}
		
		public function get wizCode():String
		{
			return _wizCode;
		}
		
		public function get defeated():Boolean
		{
			return _defeated;
		}

		public function set defeated(value:Boolean):void
		{
			_defeated = value;
		}

		public function get handle():String
		{
			return _handle;
		}

		public function get username():String
		{
			return _username;
		}
		
		public function get index():uint
		{
			return _index;
		}

	}
}
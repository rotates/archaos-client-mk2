package org.rotates.archaos.board
{
	import flash.geom.Point;
	
	import org.gestouch.core.GestureState;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.extensions.starling.StarlingUtils;
	import org.gestouch.gestures.TapGesture;
	import org.rotates.archaos.board.renderers.IBoardRenderer;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	public class Cursor extends EventDispatcher
	{
		public static const IDLE:String = "idle";
		public static const MOVE:String = "move";
		public static const UP:String = "up";
		public static const UP_RIGHT:String = "up-right";
		public static const RIGHT:String = "right";
		public static const DOWN_RIGHT:String = "down-right";
		public static const DOWN:String = "down";
		public static const DOWN_LEFT:String = "down-left";
		public static const LEFT:String = "left";
		public static const UP_LEFT:String = "up-left";
		public static const FLY:String = "fly";
		public static const ATTACK:String = "attack";
		public static const RANGED_ATTACK:String = "rangedattack";
		public static const CAST:String = "cast";
		public static const INVALID:String = "invalid";
		public static const SELECT:String = "select";
		public static const INFO:String = "info";
		public static const MOUNT:String = "mount";
		public static const DISMOUNT:String = "dismount";
		public static const WARNING:String = "warning";
		
		private var _board:Board;
		private var _renderer:IBoardRenderer;
		private var _cursorPos:Point = new Point(-1,-1);
		private var _tmpCursorPos:Point = new Point(-1,-1);
		private var _tap:TapGesture;
		
		public function Cursor(board:Board, renderer:IBoardRenderer)
		{
			this._board = board;
			this._renderer = renderer;
			_renderer.displayObject.addEventListener(TouchEvent.TOUCH, onHover);
			_tap = new TapGesture(_renderer.displayObject);
			_tap.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTap);
		}
		
		private function onHover(e:TouchEvent):void
		{
			var hover:Touch = e.getTouch(_renderer.displayObject, TouchPhase.HOVER);
			if (hover)
			{
				hover.getLocation(_renderer.displayObject, _tmpCursorPos);
				_tmpCursorPos = getCursorPos(_tmpCursorPos);
				if (_tmpCursorPos.x >= 0 && _tmpCursorPos.x < _board.boardWidth && _tmpCursorPos.y >= 0 && _tmpCursorPos.y < _board.boardHeight) {
					_renderer.cursor.visible = true;
					if (!_cursorPos.equals(_tmpCursorPos)) {
						_cursorPos.copyFrom(_tmpCursorPos);
						_board.selectIntent(_cursorPos);
						updateCursor();
					}
				}
				else {
					_renderer.cursor.visible = false;
				}
			}
		}
		
		private function onTap(e:GestureEvent):void
		{
			var pos:Point = _renderer.displayObject.globalToLocal(StarlingUtils.adjustGlobalPoint(Starling.current,_tap.location));
			pos = getCursorPos(pos);
			_cursorPos.copyFrom(pos);
			_board.selectConfirm(_cursorPos);
		}
		
		private function getCursorPos(pos:Point):Point
		{
			pos.x -= _renderer.layerOffset.x - (_renderer.tileSize); // fix board-centering offset 
			var ly:int = (((2 * pos.y - pos.x) / 2) + (_renderer.tileSize));
			var lx:int = (pos.x + ly) - (_renderer.tileSize);
			var ay:int = Math.round(ly / (_renderer.tileSize)) - 1;
			var ax:int = Math.round(lx / (_renderer.tileSize)) - 1;
			pos.x = ax;
			pos.y = ay;
			return pos;
		}
		
		public static function getCursorAngle(a:uint = 0):String {
			switch (a)
			{
				case 0:
				case 8:
					return DOWN_RIGHT;
					break;
				case 1:
					return DOWN;
					break;
				case 2:
					return DOWN_LEFT;
					break;
				case 3:
					return LEFT;
					break;
				case 4:
					return UP_LEFT;
					break;
				case 5:
					return UP;
					break;
				case 6:
					return UP_RIGHT;
					break;
				case 7:
					return RIGHT;
					break;
			}
			return IDLE;
		}
		
		public function set type(type:String):void 
		{
			_renderer.updateCursor(_cursorPos, type);
		}

		private function updateCursor():void
		{
			_renderer.updateCursor(_cursorPos);
		}
	}
}
package org.rotates.archaos.board.renderers
{
	import flash.geom.Point;
	
	import org.osflash.async.Deferred;
	import org.osflash.async.Promise;
	import org.osflash.async.when;
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Cursor;
	import org.rotates.archaos.board.Unit;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.QuadBatch;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	

	public class BoardPreview extends Sprite implements IBoardRenderer
	{
		protected var _tileSize:uint;
		
		protected var _layers:Vector.<Layer> = new Vector.<Layer>();
		protected var _units:Vector.<Unit>;
		protected var _imagePool:Object = new Object();
		protected var _w:uint;
		protected var _h:uint;
		protected var _cursor:IsoCursorRenderer;
		protected var _layerOffset:Point = new Point();
		protected var _gizmos:QuadBatch;
		
		public static const RENDERER:String = "minimap";
		
		public function BoardPreview(w:uint, h:uint, tileSize:uint = 6)
		{
			_tileSize = tileSize * Main.ASSET_SCALE_MULTIPLIER;
			_w = w;
			_h = h;
			// _units = units;
			_layers[0] = new Layer(); // tiles
			_layers[1] = new Layer(); // shadows
			_gizmos = new QuadBatch();
			_layers[2] = new Layer(); // units
			
			this.addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function get tileSize():uint
		{
			return _tileSize;
		}
		
		public function get displayObject():DisplayObject
		{
			return this;
		}
		
		protected function init(e:Event = null):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, init);

			var tile:IsoImage;
			var tileBatch:QuadBatch = new QuadBatch();
			for (var xx:uint = 0; xx < _w; xx++) {
				for (var yy:uint = 0; yy < _h; yy++) {
					tile = new IsoImage(getTileTexture(), 0, _tileSize);
					tile.smoothing = Main.TEXTURE_SMOOTHING;
					tile.x = xx;
					tile.y = yy;
					tileBatch.addImage(tile as Image);
				}
			}
			_layers[0].addChild(tileBatch);
			_layers[0].sort();
			_layers[0].flatten();
			
			for (var i:uint = 0; i < _layers.length; i++) {
				this.addChild(_layers[i]);
				_layers[i].x -= (((_h-1) * (_tileSize) * -1) - (tile.width * 0.5)) << 0; // adjust x of layers to ensure the board is centred horizontally
				_layerOffset.x = _layers[i].x;
			}
			_gizmos.x -= (((_h-1) * (_tileSize) * -1) - (tile.width * 0.5)) << 0;
			this.addChildAt(_gizmos, 2);
			
			this.pivotY = (this.height * 0.5) << 0;
			this.pivotX = (this.width * 0.5) << 0;
			
			update();
		}
		
		public function get layerOffset():Point
		{
			return _layerOffset;
		}
		
		protected function getTileTexture():Texture
		{
			return Game.ASSETS.getTexture("minimap/tile");
		}
		
		public function update(units:Vector.<Unit> = null):void
		{
			if (units != null) {
				_units = units;
			}
			_layers[1].empty();
			_layers[2].empty();
			var unit:IsoUnit;
			if (_units.length > 0) {
				for (var u:uint = 0; u < _units.length; u++) {
					unit = getIsoUnit(_units[u]);
					_layers[2].addChild(unit);
					if (unit.shadow != null) {
						_layers[1].addChild(unit.shadow);
					}
				}
			}
			if (_cursor != null) {
				_layers[2].addChild(_cursor.displayObject);
			}
			_layers[2].sort();
		}
		
		protected function getIsoUnit(unit:Unit):IsoUnit
		{
			var isoUnit:IsoUnit;
			if (!_imagePool[unit.id]) {
				isoUnit = new IsoUnit(unit, _tileSize, IsoUnit.RENDERER_MINIMAP);
				_imagePool[unit.id] = isoUnit;
			}
			else {
				isoUnit = _imagePool[unit.id];
			}
			isoUnit.color = Game.PLAYER_COLORS[unit.owner.index];
			isoUnit.x = unit.position.x;
			isoUnit.y = unit.position.y;
			return isoUnit;
		}
		
		public function get cursor():IsoCursorRenderer
		{
			return _cursor;
		}
		
		public function get gizmos():QuadBatch
		{
			return _gizmos;
		}
		
		public function updateCursor(pos:Point, type:String = null):void
		{
			_cursor.x = (pos.x - 1);
			_cursor.y = (pos.y - 1);
			if (type !== null) {
				_cursor.type = type;
			}
			_layers[2].sort();
		}
		
		public function doEffect(type:String, data:Object, callback:Function = null):Promise
		{
			// do effects here
			if (callback != null) {
				callback.call();
				return null;
			}
			else {
				return when.apply();
			}
		}
		
		public function showInfo(unit:Unit):void {
			// nothing in preview
		}
		
		public function hideInfo():void {
			// nothing in preview
		}
	}
}
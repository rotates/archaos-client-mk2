package org.rotates.archaos.board.renderers
{
	import starling.display.Image;
	import starling.textures.Texture;
	import org.rotates.archaos.Game;
	import starling.display.DisplayObject;
	
	public class IsoImage extends Image
	{
		protected var _x:Number = 0;
		protected var _y:Number = 0;
		protected var _z:Number = 0;
		private var _offset:Number = 0;
		private var _tileSize:uint;
		
		public function IsoImage(texture:Texture, offset:Number = 0, tileSize:uint = 1)
		{
			super(texture);
			this.pivotX = (this.width * 0.5) << 0;
			this.pivotY = (offset *= Main.ASSET_SCALE_MULTIPLIER) << 0;
			this.smoothing = Main.TEXTURE_SMOOTHING;
			_tileSize = tileSize;
		}
		
		public function get order():int
		{
			return (_x + _y) << 1;
		}
		
		public function get screenX():Number
		{
			return super.x;
		}
		
		public function get screenY():Number
		{
			return super.y;
		}
		
		public override function get x():Number
		{
			return _x;
		}
		
		public function get displayObject():DisplayObject
		{
			return super;
		}
		
		private function toIso():void
		{
			super.x = (_x - _y) * _tileSize;
			super.y = (((_x + _y) / 2) * _tileSize) - _z;
		}
		
		public override function set x(val:Number):void
		{			
			_x = val;
			toIso();
		}
		
		public override function get y():Number
		{
			return _y;
		}
		
		public override function set y(val:Number):void
		{
			_y = val;
			toIso();
		}
		
		public function get z():Number
		{
			return _z;
		}
		
		public function set z(val:Number):void
		{
			_z = val;
			toIso();
		}
	}
}
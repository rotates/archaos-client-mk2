package org.rotates.archaos.board.renderers
{
	import flash.geom.Point;
	
	import org.osflash.async.Promise;
	import org.rotates.archaos.board.Unit;
	
	import starling.display.DisplayObject;
	import starling.display.QuadBatch;

	public interface IBoardRenderer
	{
		function update(units:Vector.<Unit> = null):void;
		function get displayObject():DisplayObject;
		function get tileSize():uint;
		function updateCursor(pos:Point, type:String = null):void;
		function get cursor():IsoCursorRenderer;
		function get layerOffset():Point;
		function doEffect(type:String, data:Object, callback:Function = null):Promise;
		function get gizmos():QuadBatch;
		function showInfo(unit:Unit):void;
		function hideInfo():void;
	}
}
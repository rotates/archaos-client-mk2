package org.rotates.archaos.board.renderers
{
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	
	public class Layer extends Sprite
	{
		private var _name:String;
		
		public function Layer()
		{
			super();
		}
		
		public function sort():void
		{
			this.sortChildren(function(s1:IsoImage,s2:IsoImage):int {
				if (s1.order < s2.order) {
					return -1;
				}
				return 1; 
			});
			if (this.isFlattened)
			{
				this.flatten();
			}
		}

		public function empty():void
		{
			for (var i:uint = 0; i < this.numChildren; i++) {
				this.getChildAt(i).removeFromParent(true);	
			}
		}
	}
}
package org.rotates.archaos.board.renderers
{
	import feathers.controls.Callout;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Cursor;
	import org.rotates.archaos.board.Unit;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Quad;
	import org.rotates.archaos.ui.UnitInfoWindow;
	
	public class BoardRenderer extends BoardPreview implements IBoardRenderer
	{		
		public static const TILE_SIZE:uint = 14;
		public static const RENDERER:String = "main";
		
		private var infoWindow:Callout;
		
		public function BoardRenderer(w:uint, h:uint, size:uint = TILE_SIZE)
		{
			super(w, h, TILE_SIZE);
			_cursor = new IsoCursorRenderer(TILE_SIZE);
			_layers[2].addChild(_cursor.displayObject);
		}
		
		protected override function getTileTexture():Texture
		{
			return Game.ASSETS.getTexture("board/empty");
		}
		
		protected override function getIsoUnit(unit:Unit):IsoUnit
		{
			var isoUnit:IsoUnit;
			if (!_imagePool[unit.id]) {
				isoUnit = new IsoUnit(unit, TILE_SIZE * Main.ASSET_SCALE_MULTIPLIER, IsoUnit.RENDERER_MAIN);
				_imagePool[unit.id] = isoUnit;
			}
			else {
				isoUnit = _imagePool[unit.id];
			}
			return isoUnit;
		}
		
		public override function get cursor():IsoCursorRenderer
		{
			return _cursor;
		}
		
		public override function showInfo(unit:Unit):void { 
			var infoSprite:Sprite = UnitInfoWindow.create(unit);
			if (infoWindow !== null) {
				infoWindow.close(false);
			}
			infoWindow = Callout.show(infoSprite, unit.renderers[1].displayObject, Callout.DIRECTION_ANY, false);
			infoWindow.touchable = false;
			TweenMax.from(infoWindow, 0.3, {delay: 1, y: "+2", alpha: 0});
		}
		
		public override function hideInfo():void {
			if (infoWindow !== null) {
				TweenMax.to(infoWindow, 0.3, {delay: 0.3, y: "-2", alpha: 0});
			}
		}
	}
}
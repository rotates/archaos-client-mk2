package org.rotates.archaos.board.renderers
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Unit;
	
	import starling.display.Image;
	import starling.textures.Texture;
	
	
	public class IsoUnit extends IsoImage
	{
		[Embed(source = "../../../../../assets/units/classicunits.json", mimeType = "application/octet-stream")]
		private static const classicJSON:Class;
		public static const unitDisplayData:Object = JSON.parse(new classicJSON());
		
		public static const DIRECTION_LEFT:String = "l";
		public static const DIRECTION_RIGHT:String = "r";
		
		public static const RENDERER_MINIMAP:String = "minimap";
		public static const RENDERER_MAIN:String = "main";
		
		protected var _offset:int = 7;
		protected var _animFrames:Vector.<uint> = new Vector.<uint>();
		protected var _animSpeed:uint = 6;
		protected var _animDelay:int = 0;
		protected var _animIndex:uint = 0;
		protected var _renderer:String;
		
		private static const TICKER:Timer = new Timer(33);
		
		protected var _unit:Unit;
		private var _direction:String = DIRECTION_RIGHT;
		private var _shadow:IsoImage;
		private var _shadowScale:uint = 3;
		
		public function IsoUnit(unit:Unit, tileSize:uint, renderer:String = RENDERER_MAIN)
		{
			_unit = unit;
			_renderer = renderer;

			var tex:Texture;
			
			if (_renderer == RENDERER_MAIN) {
				_direction = (Math.random() > 0.5) ? DIRECTION_LEFT : DIRECTION_RIGHT;
				getDisplayData(_unit.type);
				if (_shadowScale > 0) {
					_shadow = new IsoImage(Game.ASSETS.getTexture("units/shadow-" + _shadowScale), (-6), tileSize);
					_shadow.pivotX = (_shadow.width * 0.6) << 0;
				}
				tex = getUnitTexture(_unit, direction, _animFrames.length > 1 ? _animFrames[_animIndex] : 0);
			}
			else {
				if (unit.type === "1") {
					tex = Game.ASSETS.getTexture("minimap/wizard");
					_offset = 1 * Main.ASSET_SCALE_MULTIPLIER;
				}
				else {
					tex = Game.ASSETS.getTexture("minimap/unit");
					_offset = 0;
				}
			}
			
			super(tex, _offset, tileSize);
			
			this.touchable = false;
			if (!TICKER.running) {
				TICKER.start();
			}
			if (_animFrames.length > 1 && _renderer == RENDERER_MAIN) {
				TICKER.addEventListener(TimerEvent.TIMER, onTick);	
			}
			if (_unit.dead === true) {
				dead = true;
			}
			_unit.addRenderer(this);
		}
		
		public function get shadow():Image
		{
			return _shadow;
		}
		
		public function set dead(value:Boolean):void
		{
			if (_renderer == RENDERER_MAIN) {
				if (value === true) {
					TICKER.removeEventListener(TimerEvent.TIMER, onTick);
					this.texture = getUnitTexture(_unit, _direction, "d");
				}
				else {
					TICKER.addEventListener(TimerEvent.TIMER, onTick);
					this.texture = getUnitTexture(_unit, _direction, _animFrames.length > 1 ? _animFrames[_animIndex] : 0);				
				}
			}
			else {
				this.visible = !value;
			}
		}
		
		public override function set x(val:Number):void
		{			
			super.x = val;
			if (_shadow != null) {
				_shadow.x = val;
			}
		}
		
		public override function set y(val:Number):void
		{
			super.y = val;
			if (_shadow != null) {
				_shadow.y = val;
			}
		}
		
		public override function set z(val:Number):void
		{
			super.z = val;
			if (_shadow != null) {
				_shadow.z = val * -1;
			}
		}
		
		public override function get order():int
		{
			if (_unit && _unit.dead === false) {
				return (_x + _y) << 1;
			}
			return (_x + _y) - 1 << 1;
		}
		
		private function getDisplayData(type:String):void {
			if (unitDisplayData.hasOwnProperty(type)) {
				if (unitDisplayData[type].hasOwnProperty("offY")) {
					_offset += unitDisplayData[type].offY;
				}
				if (unitDisplayData[type].hasOwnProperty("shadowScale")) {
					_shadowScale = unitDisplayData[type].shadowScale;
				}
				for (var i:uint = 0; i < unitDisplayData[type].animFrames.length; i++) {
					_animFrames[i] = unitDisplayData[type].animFrames[i];
				}
				_animSpeed = unitDisplayData[type].animSpeed;
			}
		}
		
		private static function getUnitTexture(unit:Unit, dir:String, frame:* = null):Texture
		{
			if (unit.type === "1") {
				return WizardTexture.getTexture(unit.owner.wizCode, dir);
			}
			else {
				return Game.ASSETS.getTexture("units/classic/" + unit.type + "_" + dir + "_" + (frame || "0"));
			}
		}
		
		public function get direction():String
		{
			return _direction;
		}
		
		public function set direction(value:String):void
		{
			if (_direction != value) { 
				_direction = value;
				if (_renderer == RENDERER_MAIN) {
					this.texture = getUnitTexture(_unit, _direction, _animFrames.length > 1 ? _animFrames[_animIndex] : 0);
				}
			}
		}
		
		private function onTick(e:TimerEvent = null):void
		{
			if (_unit.dead === true) {
				this.texture = getUnitTexture(_unit, _direction, "d");
				return;
			}
			if (_animDelay < _animSpeed) {
				_animDelay++;
			}
			else {
				_animDelay = 0 - Math.round(Math.random()*2);
				if (_animIndex < _animFrames.length -1) {
					_animIndex++;
				}
				else {
					_animIndex = 0;
				}
				this.texture = getUnitTexture(_unit, _direction, _animFrames[_animIndex]);
			}
		}
	}
}
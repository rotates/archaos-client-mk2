package org.rotates.archaos.board.renderers
{
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Cursor;
	
	import starling.display.DisplayObject;
	import starling.textures.Texture;
	
	public class IsoCursorRenderer extends IsoImage
	{
		private var _type:String = Cursor.IDLE;
		private var _tileSize:uint;
		
		public function IsoCursorRenderer(tileSize:uint)
		{
			super(Game.ASSETS.getTexture("cursors/idle"), 0, tileSize * Main.ASSET_SCALE_MULTIPLIER);
			_tileSize *= Main.ASSET_SCALE_MULTIPLIER;
			this.touchable = false;
		}
		
		public override function get order():int
		{
			return ((_x + _y) << 1) + 5;
		}
		
		public function get type():String
		{
			return _type;
		}
		
		public function set type(value:String):void
		{
			_type = value;
			this.texture = Game.ASSETS.getTexture("cursors/" + _type);
		}
	}
}
package org.rotates.archaos.board.renderers
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.BitmapDataChannel;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.rotates.archaos.board.Unit;
	
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class WizardTexture
	{
		[Embed(source = "../../../../../assets/wizards/wizards.png")]
		private static const WizardsAsset:Class;
		private static const WizardsBitmapData:BitmapData = (new WizardsAsset as Bitmap).bitmapData;
		
		[Embed(source = "../../../../../assets/wizards/hats.png")]
		private static const HatsAsset:Class;
		private static const HatsBitmapData:BitmapData = (new HatsAsset as Bitmap).bitmapData;
		
		[Embed(source = "../../../../../assets/wizards/colours.png")]
		private static const WizardColoursAsset:Class;
		private static const WizardColoursBitmapData:BitmapData = (new WizardColoursAsset as Bitmap).bitmapData;
		
		[Embed(source = "../../../../../assets/wizards/skintones.png")]
		private static const WizardSkinTonesAsset:Class;
		private static const WizardSkinTonesBitmapData:BitmapData = (new WizardSkinTonesAsset as Bitmap).bitmapData;
		
		private static const WIZ_WIDTH:uint = 18;
		private static const WIZ_HEIGHT:uint = 24;
		private static const HAT_Y_FIX:Vector.<uint> = new <uint>[0, 1, 2, 0, 1, 1, 0, 1, 0, 1, 2, 0, 1, 1, 0, 1];
		private static const ATLAS_COLS:uint = 56;
		
		private static var _bitmapData:BitmapData = new BitmapData(WIZ_WIDTH * ATLAS_COLS, WIZ_HEIGHT, true, 0x00000000);
		private static var _texture:Texture = Texture.fromBitmapData(_bitmapData, false);
		private static var _atlas:TextureAtlas = new TextureAtlas(_texture);
		private static var _atlasIndex:Point = new Point(0,0);
		
		public function WizardTexture()
		{
			
		}
		
		public static function getTexture(wizCode:String, dir:String = "r"):Texture
		{
			return _atlas.getTexture(wizCode + "_" + dir) || newTexture(wizCode, dir);
		}
		
		private static function outline(source:BitmapData, color:uint = 0xff000000):BitmapData
		{
			const output:BitmapData = new BitmapData(source.width, source.height, true, 0x00000000);
			const original:BitmapData = source.clone();
			const outline:BitmapData = new BitmapData(original.width, original.height, true, color);
			const rect:Rectangle = new Rectangle(0,0, source.width, source.height);
			const pt:Point = new Point(-1,-1);
			
			for (var xx:int = -1; xx < 2; xx++) {
				for (var yy:int = -1; yy < 2; yy++) {
					pt.setTo(xx,yy);
					output.copyPixels(outline, rect, pt, original, null, true);
				}				
			}
			
			pt.setTo(0,0);		
			output.copyPixels(original, rect, pt, null, null, true);
			original.dispose();
			outline.dispose();
			return output;
		}
		
		private static function newTexture(wizCode:String, dir:String):Texture
		{
			const wizValues:Object = parseWizCode(wizCode);
			
			// extract wizard sprite from wizard sprite sheet
			const wizRect:Rectangle = new Rectangle(0, 18 * wizValues.wiz, WIZ_WIDTH * 2, 18);
			var wizCanvas:BitmapData = new BitmapData(WIZ_WIDTH * 2, 18, true, 0x00000000);
			wizCanvas.copyPixels(WizardsBitmapData, wizRect, new Point(0, 0), null, null, true);
						
			// adjust colours as necessary			
			var pt:Point = new Point(0,0);
			
			const primaryDark:uint = WizardColoursBitmapData.getPixel32(wizValues.pri, 0);
			wizCanvas.threshold(wizCanvas, wizCanvas.rect, pt, "==", 0xffa10000, primaryDark);
			const primaryMid:uint = WizardColoursBitmapData.getPixel32(wizValues.pri, 1);
			wizCanvas.threshold(wizCanvas, wizCanvas.rect, pt, "==", 0xffc20000, primaryMid);
			const primaryLight:uint = WizardColoursBitmapData.getPixel32(wizValues.pri, 2);
			wizCanvas.threshold(wizCanvas, wizCanvas.rect, pt, "==", 0xffe14218, primaryLight);
			
			const secondaryDark:uint = WizardColoursBitmapData.getPixel32(wizValues.sec, 0);
			wizCanvas.threshold(wizCanvas, wizCanvas.rect, pt, "==", 0xff015d9f, secondaryDark);
			const secondaryMid:uint = WizardColoursBitmapData.getPixel32(wizValues.sec, 1);
			wizCanvas.threshold(wizCanvas, wizCanvas.rect, pt, "==", 0xff0170c1, secondaryMid);
			const secondaryLight:uint = WizardColoursBitmapData.getPixel32(wizValues.sec, 2);
			wizCanvas.threshold(wizCanvas, wizCanvas.rect, pt, "==", 0xff0190f7, secondaryLight);
			
			const skinMid:uint = WizardSkinTonesBitmapData.getPixel32(wizValues.skin, 1)
			wizCanvas.threshold(wizCanvas, wizCanvas.rect, pt, "==", 0xffff998d, skinMid);
			
			const skinLight:uint = WizardSkinTonesBitmapData.getPixel32(wizValues.skin, 0)
			wizCanvas.threshold(wizCanvas, wizCanvas.rect, pt, "==", 0xffffd6cc, skinLight);
			
			// create outline
			wizCanvas = outline(wizCanvas);
			
			// place onto dynamic atlas
			var wizAtlasPoint:Point = new Point(_atlasIndex.x * WIZ_WIDTH, _atlasIndex.y * WIZ_HEIGHT);
			
			_bitmapData.copyPixels(wizCanvas, wizCanvas.rect, wizAtlasPoint, null, null, true);
			_texture = Texture.fromBitmapData(_bitmapData, false);
			_atlas.texture = _texture;
			wizCanvas.dispose();
			
			_atlas.addRegion(wizCode + "_r", new Rectangle(wizAtlasPoint.x, wizAtlasPoint.y, WIZ_WIDTH, WIZ_HEIGHT));
			wizAtlasPoint.x += WIZ_WIDTH;
			_atlas.addRegion(wizCode + "_l", new Rectangle(wizAtlasPoint.x, wizAtlasPoint.y, WIZ_WIDTH, WIZ_HEIGHT));
			
			_atlasIndex.x += 2;
			
			if (_atlasIndex.x > ATLAS_COLS-2) {
				_atlasIndex.x = 0;
				_atlasIndex.y++;
				const newBitmapData:BitmapData = new BitmapData(WIZ_WIDTH * ATLAS_COLS, WIZ_HEIGHT * (_atlasIndex.y + 1), true, 0x00000000);
				newBitmapData.copyPixels(_bitmapData, new Rectangle(0,0,_bitmapData.width, _bitmapData.height), new Point(0,0), null, null, true);
				_bitmapData.dispose();
				_bitmapData = newBitmapData;
			}
			return _atlas.getTexture(wizCode + "_" + dir);
		}
		
		public static function get texture():Texture
		{
			return _texture;
		}
		
		// wizcode is a 10 digit hex code composed of five values in the following order:
		// 1. wizard sprite
		// 2. primary colour
		// 3. secondary colour
		// 4. skin colour
		// 5. hat (00 for none)
		
		private static function parseWizCode(wizCode:String):Object
		{
			if (wizCode.length != 10) {
				throw new Error("Invalid wizCode");
			}
			var output:Object = new Object();
			output.wiz = parseInt(wizCode.substr(0,2),16);
			output.pri = parseInt(wizCode.substr(2,2),16);
			output.sec = parseInt(wizCode.substr(4,2),16);
			output.skin = parseInt(wizCode.substr(6,2),16);
			output.hat = parseInt(wizCode.substr(8,2),16);
			return output;
		}
	}
}
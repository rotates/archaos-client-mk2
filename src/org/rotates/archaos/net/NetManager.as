package org.rotates.archaos.net
{		
	public class NetManager
	{
		public static const HTTP:String = "http";
		public static const TCP:String = "tcp";
		public static const DATA:String = "data";
		
		private var _ip:String = "localhost";
		private var _port:uint = 3000;
		private var _currentTime:uint;
		private var _callback:Function;
		private var _client:INetClient;
		private var _mode:String;
		
		public function NetManager(ip:String, mode:String = NetManager.TCP)
		{
			_ip = ip;
			this.mode = mode;
		}
		
		public function set mode(value:String):void
		{
			_mode = value;
			_port = (_mode == NetManager.TCP) ? 3001 : 3000;
			
			if (_mode == NetManager.TCP) {
				_client = new TCPClient(_ip, _port);
			}
			else {
				_client = new HTTPClient(_ip, _port);
			}
		}
		
		public function get mode():String
		{
			return _mode;
		}
		
		public function get client():INetClient
		{
			return _client;
		}
	}
}
package org.rotates.archaos.net
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class NetCallback
	{
		private var _callback:Function;
		private var _data:Object;
		private var _id:uint = 0;
		private var _timestamp:uint = new Date().time;
		
		private static const TIMEOUT:uint = 5000;
		
		private static var _id:uint = 1;
		private static var _list:Vector.<NetCallback> = new Vector.<NetCallback>();
		private static var _timeoutTimer:Timer = new Timer(5000);		
		
		public function NetCallback(id:uint, data:Object = null, callback:Function = null)
		{
			this._id = id;
			this._data = data;
			this._callback = callback;
			if (_timeoutTimer.running == false) {
				_timeoutTimer.start();
				_timeoutTimer.addEventListener(TimerEvent.TIMER, cullCallbacks);
			}
		}
		
		private static function cullCallbacks(e:TimerEvent = null):void
		{
			const currentTime:uint = new Date().time;
			if (NetCallback._list.length > 0) {
				for (var i:uint = 0; i < NetCallback._list.length; i++) {
					if (NetCallback._list[i].timestamp < currentTime-TIMEOUT) {
						trace("Culling NetCallback", NetCallback._list[i].id);
						NetCallback._list.splice(i, 1)[0];
					}
				}
			}
		}
		
		public function set data(value:Object):void
		{
			this._data = value;
		}
		
		public function get data():Object
		{
			return this._data;
		}
		
		public function set callback(value:Function):void
		{
			this._callback = value;
		}
		
		public function get callback():Function
		{
			return this._callback;
		}
		
		public function get id():uint
		{
			return this._id;
		}
		
		public function get timestamp():uint
		{
			return _timestamp;
		}
		
		public static function create(data:Object = null, callback:Function = null):NetCallback
		{
			var newItem:NetCallback = new NetCallback(NetCallback._id++, data, callback);
			NetCallback._list.push(newItem);
			return newItem;
		}
		
		public static function retrieve(id:uint):NetCallback
		{
			var retrieved:NetCallback;
			if (NetCallback._list.length > 0) {
				for (var i:uint = 0; i < NetCallback._list.length; i++) {
					if (NetCallback._list[i].id == id) {
						return NetCallback._list.splice(i, 1)[0];
					}
				}
			}
			return null;
		}
	}
}
package org.rotates.archaos.net
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.Security;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.actions.LocalAction;
	
	import starling.events.EventDispatcher;
	import starling.events.Event;

	public class HTTPClient extends EventDispatcher implements INetClient
	{
		private var _port:uint;
		private var _ip:String;
		private var _request:URLRequest;
		private var _loader:URLLoader;
		private var _callback:Function;
		private var _data:Object;
		private var _server:String;
		
		public function HTTPClient(ip:String, port:uint)
		{
			_ip = ip;
			_port = port;
			_server = "http://" + _ip + ":" + _port.toString();
			
			Security.loadPolicyFile(_server + "/crossdomain.xml");
			
			_request = new URLRequest();
			_loader = new URLLoader();
			_loader.addEventListener(flash.events.Event.COMPLETE, dataHandler);
		}
		
		private function getData(action:String, callback:Function):void
		{
			_request.url = _server + action;
			_loader.load(_request);
			_callback = callback;
		}
		
		public function login(username:String, password:String, callback:Function):void
		{
			getData("/server/login?username=" + username + "&password=" + password, callback);
		}
		
		public function ident(callback:Function):void // maps a user to a socket connection and does not need to be called when using HTTP
		{
			callback.call(null, {response: {ident: true}});
		}
		
		public function getUnitDefs(callback:Function):void
		{
			getData("/data/units", callback);
		}
		
		public function getGames(callback:Function):void
		{
			getData("/games/?username=" + Game.username + "&token=" + Game.token, callback);	
		}
		
		public function getGame(id:String, callback:Function):void
		{
			getData("/games/" + id + "/?username=" + Game.username + "&token=" + Game.token, callback);	
		}
		
		public function getGameActions(id:String, since:uint, callback:Function):void
		{
			getData("/games/" + id + "/update/?username=" + Game.username + "&token=" + Game.token + "&since=" + since, callback);
		}
		
		public function sendAction(id:String, type:String, data:Object, callback:Function):void
		{
			switch (type) {
				case LocalAction.MOVED:
					if (data.hasOwnProperty("path")) {
						getData("/games/" + id + "/moved/?username=" + Game.username + "&token=" + Game.token + "&unit=" + data.unit + "&path=" + data.path.map(function(p:Object, i:int, arr:Array):String { return p.x + "-" + p.y; }).join(","), callback);
					}
					else if (data.hasOwnProperty("to")) {
						getData("/games/" + id + "/moved/?username=" + Game.username + "&token=" + Game.token + "&unit=" + data.unit + "&x=" + data.to.x + "&y=" + data.to.y, callback);
					}
					break;
			}
		}
		
		private function dataHandler(e:flash.events.Event):void
		{
			var data:Object = JSON.parse(_loader.data);
			_data = data;
			if (_callback != null) {
				_callback.call(null, _data);
			}
		}
	}
}
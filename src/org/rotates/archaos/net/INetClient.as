package org.rotates.archaos.net
{
	import starling.events.Event;

	public interface INetClient
	{
		function login(username:String, password:String, callback:Function):void;
		function ident(callback:Function):void;
		function getUnitDefs(callback:Function):void;
		function getGames(callback:Function):void;
		function getGame(id:String, callback:Function):void;
		function getGameActions(id:String, since:uint, callback:Function):void;
		function sendAction(id:String, type:String, data:Object, callback:Function):void;
		function addEventListener(type:String, listener:Function):void;
	}
}
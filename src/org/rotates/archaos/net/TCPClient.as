package org.rotates.archaos.net
{
	import flash.events.*;
	import flash.net.Socket;
	import flash.system.Security;
	import flash.utils.ByteArray;
	
	import org.msgpack.MsgPack;
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.actions.LocalAction;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	public class TCPClient extends starling.events.EventDispatcher implements INetClient
	{		
		private var _port:uint;
		private var _ip:String;
		private var _callback:Function;
		private var _data:Object;
		private var _server:String;
		private var _socket:Socket;
		private var _sid:uint = 1;
		private var _msgpack:MsgPack = new MsgPack();
		private var _socketBuffer:ByteArray = new ByteArray();
		
		public function TCPClient(ip:String, port:uint)
		{
			_ip = ip;
			_port = port;
			_server = "http://" + _ip + ":" + _port.toString();
			
			connect();
		}
		
		private function connect():void
		{
			_socket = new Socket(_ip, _port);
			_socket.timeout = 3;
			_socket.addEventListener(flash.events.Event.CONNECT, socketConnect);
			_socket.addEventListener(IOErrorEvent.IO_ERROR, socketError);
		}
		
		public function login(username:String, password:String, callback:Function):void
		{
			var object:Object = {
				sys: "server",
				action: "login",
				username: username,
				password: password
			};
			getData(object, callback);
		}
		
		public function ident(callback:Function):void
		{
			var object:Object = {
				sys: "server",
				action: "ident",
				username: Game.username,
				token: Game.token
			};
			getData(object, callback);
		}
		
		public function getUnitDefs(callback:Function):void
		{
			var object:Object = {
				sys: "data",
				type: "units"
			};
			getData(object, callback);
		}
		
		public function getGames(callback:Function):void
		{
			var object:Object = {
				sys: "games",
				username: Game.username,
				token: Game.token
			};
			
			getData(object, callback);	
		}
		
		public function getGame(id:String, callback:Function):void
		{
			var object:Object = {
				sys: "games",
				game: id,
				username: Game.username,
				token: Game.token
			};
			
			getData(object, callback);	
		}
		
		public function getGameActions(id:String, since:uint, callback:Function):void
		{
			var object:Object = {
				sys: "games",
				action: "update",
				game: id,
				username: Game.username,
				token: Game.token
			};
			if (since) {
				object.since = since;
			}
			getData(object, callback);
		}
		
		public function sendAction(id:String, type:String, data:Object, callback:Function):void
		{
			var object:Object = {
				sys: "games",
				game: id,
				username: Game.username,
				token: Game.token,
				action: type
			};
			switch (type) {
				case LocalAction.MOVED:
					if (data.hasOwnProperty("path")) {
						object.unit = data.unit;
						object.path = data.path.map(function(p:Object, i:int, arr:Array):String { return p.x + "-" + p.y; }).join(",");
						getData(object, callback);
					}
					else if (data.hasOwnProperty("to")) {
						object.unit = data.unit;
						object.x = data.to.x;
						object.y = data.to.y;
						getData(object, callback);
					}
					break;
				case LocalAction.ATTACKED:
					object.unit = data.unit;
					object.target = data.target;
					getData(object, callback);
					break;
			}
		}
		
		private function socketConnect(e:flash.events.Event):void
		{
			trace("TCP Connected");
			_socket.removeEventListener(flash.events.Event.CONNECT, socketConnect);
			_socket.removeEventListener(IOErrorEvent.IO_ERROR, socketError);
			_socket.addEventListener(ProgressEvent.SOCKET_DATA, socketData);
			_socket.addEventListener(flash.events.Event.CLOSE, socketClose);
			if (Game.username != null) {
				ident(function(data:Object):void {
					if (data.response.hasOwnProperty("error")) {
						Game.showLogin();
					}
				});
			}
		}
		
		private function socketError(e:IOErrorEvent):void
		{
			trace("TCP Connect failed, attempting reconnect");
			_socket.removeEventListener(flash.events.Event.CONNECT, socketConnect);
			_socket.removeEventListener(IOErrorEvent.IO_ERROR, socketError);
			connect();
		}
		
		private function socketClose(e:flash.events.Event):void
		{
			trace("TCP Connection closed, attempting reconnect");
			_socket.close();
			_socket.removeEventListener(flash.events.Event.CLOSE, socketClose);
			_socket.removeEventListener(ProgressEvent.SOCKET_DATA, socketData);
			connect();
		}
		
		private function socketData(e:ProgressEvent):void
		{
			var byte:int;
			while (_socket.bytesAvailable) {
				byte = _socket.readByte();
				if (byte !== 0) {
					_socketBuffer.writeByte(byte);
				}
				else {
					var data:Object = JSON.parse(_socketBuffer.toString());
					trace("TCP <:", JSON.stringify(data));
					
					this.dispatchEventWith(starling.events.Event.CHANGE, false, data);
					
					_socketBuffer.clear();
					
					if (data.hasOwnProperty("response") && data.response.hasOwnProperty("sid")) {
						var ncb:NetCallback = NetCallback.retrieve(data.response.sid);
						if (ncb != null) {
							ncb.callback.call(null, data);
						}
					}
				}
			}
		}
				
		private function getData(action:Object, callback:Function):void
		{
			var ncb:NetCallback = NetCallback.create(null, callback);
			action.sid = ncb.id;
			trace("TCP >:", JSON.stringify(action));
			
			// var byteArray:ByteArray = MessagePack.encoder.write(action);
			var byteArray:ByteArray = _msgpack.write(action);
			
			byteArray.position = 0;
			_socket.writeBytes(byteArray, 0, byteArray.bytesAvailable);
			_socket.flush();
		}
	}
}
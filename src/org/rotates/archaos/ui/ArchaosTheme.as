package org.rotates.archaos.ui
{
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Check;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Radio;
	import feathers.controls.SimpleScrollBar;
	import feathers.controls.TextInput;
	import feathers.controls.renderers.BaseDefaultItemRenderer;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.controls.text.StageTextTextEditor;
	import feathers.core.DisplayListWatcher;
	import feathers.core.FeathersControl;
	import feathers.display.Scale3Image;
	import feathers.display.Scale9Image;
	import feathers.display.TiledImage;
	import feathers.skins.ImageStateValueSelector;
	import feathers.skins.Scale9ImageStateValueSelector;
	import feathers.text.BitmapFontTextFormat;
	import feathers.textures.Scale3Textures;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	import flash.text.TextFormatAlign;
	
	import org.rotates.archaos.Game;
	
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.textures.TextureSmoothing;
	
	public class ArchaosTheme extends DisplayListWatcher
	{
		private static const COMPONENT_SIZE:uint = 26 * Main.ASSET_SCALE_MULTIPLIER;
		private static const HEADER_HEIGHT:uint = 28 * Main.ASSET_SCALE_MULTIPLIER;
		private static const BUTTON_SIZE:uint = 20 * Main.ASSET_SCALE_MULTIPLIER;
		
		protected var backgroundTexture:Texture;
		protected var headerTexture:Texture;
		
		protected var buttonUpTexture:Scale9Textures;
		protected var buttonHoverTexture:Scale9Textures;
		protected var headerButtonUpTexture:Scale9Textures;
		protected var headerButtonHoverTexture:Scale9Textures;
		protected var headerButtonLeftUpTexture:Scale9Textures;
		protected var headerButtonLeftHoverTexture:Scale9Textures;
		protected var headerButtonRightUpTexture:Scale9Textures;
		protected var headerButtonRightHoverTexture:Scale9Textures;
		public static const BUTTON_GRID:Rectangle = new Rectangle(4,4,1,1);
		
		protected var radioUpTexture:Texture;
		protected var radioSelectedTexture:Texture;
		protected var radioDownTexture:Texture;
		protected var radioDisabledTexture:Texture;
		
		protected var checkUpTexture:Texture;
		protected var checkSelectedTexture:Texture;
		protected var checkDownTexture:Texture;
		protected var checkDisabledTexture:Texture;
		
		protected var calloutBackgroundTexture:Scale9Textures;
		protected var calloutSelectedBackgroundTexture:Scale9Textures;
		protected var calloutHoverBackgroundTexture:Scale9Textures;
		public static const CALLOUT_GRID:Rectangle = new Rectangle(3,3,1,1);
		protected var calloutArrowTopTexture:Texture;
		protected var calloutArrowBottomTexture:Texture;
		protected var calloutArrowLeftTexture:Texture;
		protected var calloutArrowRightTexture:Texture;
		
		protected var scrollVerticalTexture:Scale3Textures;
		
		protected var textInputTexture:Scale9Textures;
		protected var textInputActiveTexture:Scale9Textures;
		public static const TEXTINPUT_GRID:Rectangle = new Rectangle(3,3,1,1);
		
		protected var largeFont:BitmapFont;
		public static const LARGE_FONT_SIZE:Number = 16 * Main.ASSET_SCALE_MULTIPLIER;
		
		protected var smallFont:BitmapFont;
		public static const SMALL_FONT_SIZE:Number = 13 * Main.ASSET_SCALE_MULTIPLIER;
		
		private var uiAtlas:TextureAtlas;
		
		protected static function textRendererFactory():BitmapFontTextRenderer
		{
			const renderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
			renderer.smoothing = Main.TEXTURE_SMOOTHING;
			return renderer;
		}
		
		protected function imageLoaderFactory():ImageLoader
		{
			const image:ImageLoader = new ImageLoader();
			image.smoothing = Main.TEXTURE_SMOOTHING;
			return image;
		}
		
		protected static function textEditorFactory():StageTextTextEditor
		{
			return new StageTextTextEditor();
		}		
		
		public function ArchaosTheme(root:DisplayObjectContainer)
		{
			super(root);
			
			BUTTON_GRID.x = BUTTON_GRID.y *= Main.ASSET_SCALE_MULTIPLIER
			BUTTON_GRID.height = BUTTON_GRID.width *= Main.ASSET_SCALE_MULTIPLIER;
			
			CALLOUT_GRID.x = CALLOUT_GRID.y *= Main.ASSET_SCALE_MULTIPLIER
			CALLOUT_GRID.height = CALLOUT_GRID.width *= Main.ASSET_SCALE_MULTIPLIER;
			
			TEXTINPUT_GRID.x = TEXTINPUT_GRID.y *= Main.ASSET_SCALE_MULTIPLIER
			TEXTINPUT_GRID.height = TEXTINPUT_GRID.width *= Main.ASSET_SCALE_MULTIPLIER;

			backgroundTexture = Game.ASSETS.getTexture("ui/background");
			headerTexture = Game.ASSETS.getTexture("ui/header");
			
			buttonUpTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/button-green"), BUTTON_GRID);
			buttonHoverTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/button-green-hover"), BUTTON_GRID);
			
			headerButtonUpTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/button-header"), BUTTON_GRID);
			headerButtonHoverTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/button-header-hover"), BUTTON_GRID);
			headerButtonLeftUpTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/button-header-left"), BUTTON_GRID);
			headerButtonLeftHoverTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/button-header-left-hover"), BUTTON_GRID);
			headerButtonRightUpTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/button-header-right"), BUTTON_GRID);
			headerButtonRightHoverTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/button-header-right-hover"), BUTTON_GRID);
			
			radioUpTexture = Game.ASSETS.getTexture("ui/radio-unchecked");
			radioSelectedTexture = Game.ASSETS.getTexture("ui/radio-checked");
			radioDownTexture = Game.ASSETS.getTexture("ui/radio-hover");
			radioDisabledTexture = Game.ASSETS.getTexture("ui/radio-disabled");
			
			checkUpTexture = Game.ASSETS.getTexture("ui/check-unchecked");
			checkSelectedTexture = Game.ASSETS.getTexture("ui/check-checked");
			checkDownTexture = Game.ASSETS.getTexture("ui/check-hover");
			checkDisabledTexture = Game.ASSETS.getTexture("ui/check-disabled");
			
			calloutBackgroundTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/callout"), CALLOUT_GRID);
			calloutSelectedBackgroundTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/callout-selected"), CALLOUT_GRID);
			calloutHoverBackgroundTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/callout-hover"), CALLOUT_GRID);
			calloutArrowTopTexture = Game.ASSETS.getTexture("ui/callout-arrow-up");
			calloutArrowBottomTexture = Game.ASSETS.getTexture("ui/callout-arrow-down");
			calloutArrowLeftTexture = Game.ASSETS.getTexture("ui/callout-arrow-left");
			calloutArrowRightTexture = Game.ASSETS.getTexture("ui/callout-arrow-right");
			
			scrollVerticalTexture = new Scale3Textures(Game.ASSETS.getTexture("ui/scroll-inactive"), 2 * Main.ASSET_SCALE_MULTIPLIER, 9 * Main.ASSET_SCALE_MULTIPLIER, Scale3Textures.DIRECTION_VERTICAL);
			
			textInputTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/edit-inactive"), TEXTINPUT_GRID);
			textInputActiveTexture = new Scale9Textures(Game.ASSETS.getTexture("ui/edit-active"), TEXTINPUT_GRID);
			
			largeFont = TextField.getBitmapFont("Chaos Sans");
			smallFont = TextField.getBitmapFont("Archaos Sans");
			
			FeathersControl.defaultTextRendererFactory = textRendererFactory;
			FeathersControl.defaultTextEditorFactory = textEditorFactory;
			
			this.setInitializerForClass(Button, buttonInit);
			this.setInitializerForClass(Button, headerButtonInit, Header.DEFAULT_CHILD_NAME_ITEM);
			this.setInitializerForClass(Button, headerButtonLeftInit, "button-back");
			this.setInitializerForClass(Button, headerButtonRightInit, "button-forward");
			this.setInitializerForClass(Radio, radioInit);
			this.setInitializerForClass(Check, checkInit);
			this.setInitializerForClass(Callout, calloutInit);
			
			this.setInitializerForClass(Label, labelInit);
			this.setInitializerForClass(Label, labelCenteredInit, "centered");
			this.setInitializerForClass(Label, labelErrorInit, "error");
			
			this.setInitializerForClass(Label, labelBigInit, "big");
			this.setInitializerForClass(Label, labelCenteredBigInit, "big-centered");
			
			this.setInitializerForClass(DefaultListItemRenderer, itemRendererInit);
			this.setInitializerForClass(List, listInit);
			this.setInitializerForClass(Button, simpleScrollBarThumbInit, SimpleScrollBar.DEFAULT_CHILD_NAME_THUMB);
			
			this.setInitializerForClass(TextInput, textInputInit);
			this.setInitializerForClass(TextInput, largeTextInputInit, "big");
			
			this.setInitializerForClass(Header, headerInit);
		}
		
		protected function headerInit(header:Header):void
		{
			header.minWidth = COMPONENT_SIZE;
			header.minHeight = HEADER_HEIGHT;
			header.maxHeight = HEADER_HEIGHT;
			header.paddingTop = header.paddingRight = header.paddingBottom = header.paddingLeft = 4 * Main.ASSET_SCALE_MULTIPLIER;
			header.gap = 8 * Main.ASSET_SCALE_MULTIPLIER;
			const backgroundSkin:TiledImage = new TiledImage(headerTexture);
			backgroundSkin.smoothing = TextureSmoothing.NONE;
			backgroundSkin.width = 28 * Main.ASSET_SCALE_MULTIPLIER;
			backgroundSkin.height = 28 * Main.ASSET_SCALE_MULTIPLIER;
			header.backgroundSkin = backgroundSkin;
			header.titleProperties.textFormat = new BitmapFontTextFormat(largeFont, LARGE_FONT_SIZE, 0xffffff);
		}
		
		protected function itemRendererInit(renderer:BaseDefaultItemRenderer):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = calloutBackgroundTexture;
			skinSelector.defaultSelectedValue = calloutSelectedBackgroundTexture;
			skinSelector.setValueForState(calloutHoverBackgroundTexture, Button.STATE_HOVER, false);
			skinSelector.setValueForState(calloutHoverBackgroundTexture, Button.STATE_DOWN, false);
			skinSelector.imageProperties =
				{
					width: COMPONENT_SIZE,
					height: COMPONENT_SIZE,
					smoothing: Main.TEXTURE_SMOOTHING
				};
			renderer.stateToSkinFunction = skinSelector.updateValue;
			
			if (Main.MOBILE === true) {
				renderer.defaultLabelProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xcccccc);
				renderer.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xffff00);				
			}
			else {
				renderer.defaultLabelProperties.textFormat = new BitmapFontTextFormat(largeFont, LARGE_FONT_SIZE, 0xcccccc);
				renderer.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat(largeFont, LARGE_FONT_SIZE, 0xffff00);
			}
			renderer.accessoryLabelProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0x888888);
			
			renderer.paddingTop = renderer.paddingBottom = 4 * Main.ASSET_SCALE_MULTIPLIER;
			renderer.paddingLeft = renderer.paddingRight = 6 * Main.ASSET_SCALE_MULTIPLIER;
			
			renderer.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
			renderer.minWidth = renderer.minHeight = COMPONENT_SIZE;
			
			renderer.gap = 10 * Main.ASSET_SCALE_MULTIPLIER;
			renderer.iconPosition = Button.ICON_POSITION_LEFT;
			renderer.accessoryGap = Number.POSITIVE_INFINITY;
			renderer.accessoryPosition = BaseDefaultItemRenderer.ACCESSORY_POSITION_RIGHT;
			
			renderer.accessoryLoaderFactory = this.imageLoaderFactory;
			renderer.iconLoaderFactory = this.imageLoaderFactory;
		}
		
		protected function simpleScrollBarThumbInit(thumb:Button):void
		{
			const defaultSkin:Scale3Image = new Scale3Image(scrollVerticalTexture);
			defaultSkin.smoothing = Main.TEXTURE_SMOOTHING;
			thumb.defaultSkin = defaultSkin;
			
			thumb.minTouchWidth = thumb.minTouchHeight = 12 * Main.ASSET_SCALE_MULTIPLIER;
		}
		
		protected function listInit(list:List):void
		{
			const backgroundSkin:TiledImage = new TiledImage(backgroundTexture);
			backgroundSkin.smoothing = TextureSmoothing.NONE;;
			list.backgroundSkin = backgroundSkin;
			list.scrollerProperties.snapScrollPositionsToPixels = true;
		}
		
		protected function labelInit(label:Label):void
		{
			label.textRendererProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xffffff);
			label.textRendererProperties.snapToPixels = true;
			label.textRendererProperties.wordWrap = true;
		}
		
		protected function labelBigInit(label:Label):void
		{
			label.textRendererProperties.textFormat = new BitmapFontTextFormat(largeFont, LARGE_FONT_SIZE, 0xffffff);
			label.textRendererProperties.snapToPixels = true;
			label.textRendererProperties.wordWrap = true;
		}
		
		protected function labelCenteredInit(label:Label):void
		{
			label.textRendererProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xffffff, TextFormatAlign.CENTER);
			label.textRendererProperties.snapToPixels = true;
			label.textRendererProperties.wordWrap = true;
		}
		
		protected function labelErrorInit(label:Label):void
		{
			label.textRendererProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xff0000, TextFormatAlign.CENTER);
			label.textRendererProperties.snapToPixels = true;
			label.textRendererProperties.wordWrap = true;
		}
		
		protected function labelCenteredBigInit(label:Label):void
		{
			label.textRendererProperties.textFormat = new BitmapFontTextFormat(largeFont, LARGE_FONT_SIZE, 0xffffff, TextFormatAlign.CENTER);
			label.textRendererProperties.snapToPixels = true;
			label.textRendererProperties.wordWrap = true;
		}
		
		protected function buttonInit(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = buttonUpTexture;
			skinSelector.setValueForState(buttonHoverTexture, Button.STATE_HOVER, false);
			skinSelector.setValueForState(buttonHoverTexture, Button.STATE_DOWN, false);
			skinSelector.imageProperties = {
				width: COMPONENT_SIZE,
				height: COMPONENT_SIZE - (4 * Main.ASSET_SCALE_MULTIPLIER),
				smoothing: Main.TEXTURE_SMOOTHING
			};
			button.stateToSkinFunction = skinSelector.updateValue;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(largeFont, LARGE_FONT_SIZE, 0xffffff);
			button.paddingTop = button.paddingBottom = button.paddingLeft = button.paddingRight = 6 * Main.ASSET_SCALE_MULTIPLIER;
			button.gap = 3 * Main.ASSET_SCALE_MULTIPLIER;
			button.minWidth = button.minHeight = COMPONENT_SIZE * Main.ASSET_SCALE_MULTIPLIER;
			button.minTouchWidth = button.minTouchHeight = COMPONENT_SIZE * Main.ASSET_SCALE_MULTIPLIER;
			button.useHandCursor = true;
		}
		
		protected function headerButtonInit(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = headerButtonUpTexture;
			skinSelector.setValueForState(headerButtonHoverTexture, Button.STATE_HOVER, false);
			skinSelector.setValueForState(headerButtonHoverTexture, Button.STATE_DOWN, false);
			skinSelector.imageProperties = {
				width: COMPONENT_SIZE,
					height: BUTTON_SIZE,
					smoothing: Main.TEXTURE_SMOOTHING
			};
			button.stateToSkinFunction = skinSelector.updateValue; 
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xffffff);
			button.paddingTop = button.paddingBottom = 3 * Main.ASSET_SCALE_MULTIPLIER; 
			button.paddingLeft = button.paddingRight = 6 * Main.ASSET_SCALE_MULTIPLIER;
			button.gap = 3 * Main.ASSET_SCALE_MULTIPLIER;
			button.minWidth = button.minHeight = BUTTON_SIZE;
			button.minTouchWidth = button.minTouchHeight = BUTTON_SIZE;
			button.useHandCursor = true;
		}
		
		protected function headerButtonLeftInit(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = headerButtonLeftUpTexture;
			skinSelector.setValueForState(headerButtonLeftHoverTexture, Button.STATE_HOVER, false);
			skinSelector.setValueForState(headerButtonLeftHoverTexture, Button.STATE_DOWN, false);
			skinSelector.imageProperties = {
				width: COMPONENT_SIZE,
					height: BUTTON_SIZE,
					smoothing: Main.TEXTURE_SMOOTHING
			};
			button.stateToSkinFunction = skinSelector.updateValue; 
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xffffff);
			button.paddingTop = button.paddingBottom = 3 * Main.ASSET_SCALE_MULTIPLIER; 
			button.paddingLeft = button.paddingRight = 6* Main.ASSET_SCALE_MULTIPLIER;
			button.gap = 3 * Main.ASSET_SCALE_MULTIPLIER;
			button.minWidth = button.minHeight = BUTTON_SIZE;
			button.minTouchWidth = button.minTouchHeight = BUTTON_SIZE;
			button.useHandCursor = true;
		}
		
		protected function headerButtonRightInit(button:Button):void
		{
			const skinSelector:Scale9ImageStateValueSelector = new Scale9ImageStateValueSelector();
			skinSelector.defaultValue = headerButtonRightUpTexture;
			skinSelector.setValueForState(headerButtonRightHoverTexture, Button.STATE_HOVER, false);
			skinSelector.setValueForState(headerButtonRightHoverTexture, Button.STATE_DOWN, false);
			skinSelector.imageProperties = {
				width: COMPONENT_SIZE,
					height: BUTTON_SIZE,
					smoothing: Main.TEXTURE_SMOOTHING
			};
			button.stateToSkinFunction = skinSelector.updateValue; 
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xffffff);
			button.paddingTop = button.paddingBottom = 3 * Main.ASSET_SCALE_MULTIPLIER; 
			button.paddingLeft = button.paddingRight = 6 * Main.ASSET_SCALE_MULTIPLIER;
			button.gap = 3 * Main.ASSET_SCALE_MULTIPLIER;
			button.minWidth = button.minHeight = BUTTON_SIZE;
			button.minTouchWidth = button.minTouchHeight = BUTTON_SIZE;
			button.useHandCursor = true;
		}
		
		protected function radioInit(radio:Radio):void
		{
			const iconSelector:ImageStateValueSelector = new ImageStateValueSelector();
			iconSelector.defaultValue = radioUpTexture;
			iconSelector.defaultSelectedValue = radioSelectedTexture;
			iconSelector.setValueForState(radioDownTexture, Button.STATE_DOWN, false);
			iconSelector.setValueForState(radioDisabledTexture, Button.STATE_DISABLED, false);
			iconSelector.setValueForState(radioDownTexture, Button.STATE_DOWN, true);
			iconSelector.setValueForState(radioDownTexture, Button.STATE_DISABLED, true);
			iconSelector.imageProperties = {
				smoothing: Main.TEXTURE_SMOOTHING
			};
			radio.stateToIconFunction = iconSelector.updateValue;
			radio.defaultLabelProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xcccccc);
			radio.gap = 6 * Main.ASSET_SCALE_MULTIPLIER;
			radio.minTouchWidth = radio.minTouchHeight = COMPONENT_SIZE;
			radio.useHandCursor = true;
		}
		
		protected function checkInit(check:Check):void
		{
			const iconSelector:ImageStateValueSelector = new ImageStateValueSelector();
			iconSelector.defaultValue = checkUpTexture;
			iconSelector.defaultSelectedValue = checkSelectedTexture;
			iconSelector.setValueForState(checkDownTexture, Button.STATE_DOWN, false);
			iconSelector.setValueForState(checkDisabledTexture, Button.STATE_DISABLED, false);
			iconSelector.setValueForState(checkDownTexture, Button.STATE_DOWN, true);
			iconSelector.setValueForState(checkDownTexture, Button.STATE_DISABLED, true);
			iconSelector.imageProperties = {
				smoothing: Main.TEXTURE_SMOOTHING
			};
			check.stateToIconFunction = iconSelector.updateValue;
			check.defaultLabelProperties.textFormat = new BitmapFontTextFormat(smallFont, SMALL_FONT_SIZE, 0xcccccc);
			check.gap = 6 * Main.ASSET_SCALE_MULTIPLIER;
			check.minTouchWidth = check.minTouchHeight = COMPONENT_SIZE;
			check.useHandCursor = true;
		}
		
		protected function calloutInit(callout:Callout):void
		{
			const backgroundSkin:Scale9Image = new Scale9Image(calloutBackgroundTexture);
			backgroundSkin.smoothing = Main.TEXTURE_SMOOTHING;
			callout.backgroundSkin = backgroundSkin;
			
			const topArrowSkin:Image = new Image(calloutArrowTopTexture);
			topArrowSkin.smoothing = Main.TEXTURE_SMOOTHING;
			callout.topArrowSkin = topArrowSkin;
			
			const bottomArrowSkin:Image = new Image(calloutArrowBottomTexture);
			bottomArrowSkin.smoothing = Main.TEXTURE_SMOOTHING;
			callout.bottomArrowSkin = bottomArrowSkin;
			
			callout.topArrowGap = callout.bottomArrowGap = -1 * Main.ASSET_SCALE_MULTIPLIER;
			
			const leftArrowSkin:Image = new Image(calloutArrowLeftTexture);
			leftArrowSkin.smoothing = Main.TEXTURE_SMOOTHING;
			callout.leftArrowSkin = leftArrowSkin;
			
			const rightArrowSkin:Image = new Image(calloutArrowRightTexture);
			rightArrowSkin.smoothing = Main.TEXTURE_SMOOTHING;
			callout.rightArrowSkin = rightArrowSkin;
			
			callout.leftArrowGap = callout.rightArrowGap = -1 * Main.ASSET_SCALE_MULTIPLIER;
			
			callout.paddingTop = callout.paddingBottom = callout.paddingLeft = callout.paddingRight = 6 * Main.ASSET_SCALE_MULTIPLIER;
			Callout.stagePaddingBottom = Callout.stagePaddingTop = Callout.stagePaddingLeft = Callout.stagePaddingRight = 2 * Main.ASSET_SCALE_MULTIPLIER;
		}
		
		protected function textInputInit(input:TextInput):void
		{
			const backgroundSkin:Scale9Image = new Scale9Image(textInputTexture);
			backgroundSkin.smoothing = Main.TEXTURE_SMOOTHING;
			input.backgroundSkin = backgroundSkin;
			
			const backgroundFocusedSkin:Scale9Image = new Scale9Image(textInputActiveTexture);
			backgroundFocusedSkin.smoothing = Main.TEXTURE_SMOOTHING;
			input.backgroundFocusedSkin = backgroundFocusedSkin;
			
			input.minWidth = input.minHeight = (COMPONENT_SIZE * 0.75) << 0;
			input.minTouchWidth = input.minTouchHeight = COMPONENT_SIZE;
			input.paddingTop = input.paddingBottom = input.paddingLeft = input.paddingRight = 4 * Main.ASSET_SCALE_MULTIPLIER;
			
			input.textEditorProperties.fontFamily = "Helvetica";
			input.paddingTop = input.paddingBottom = 2 * Main.ASSET_SCALE_MULTIPLIER;
			input.textEditorProperties.fontSize = 10 * Main.ASSET_SCALE_MULTIPLIER;
			input.textEditorProperties.color = 0xffffff;
		}
		
		protected function largeTextInputInit(input:TextInput):void
		{
			const backgroundSkin:Scale9Image = new Scale9Image(textInputTexture);
			backgroundSkin.smoothing = Main.TEXTURE_SMOOTHING;
			input.backgroundSkin = backgroundSkin;
			
			const backgroundFocusedSkin:Scale9Image = new Scale9Image(textInputActiveTexture);
			backgroundFocusedSkin.smoothing = Main.TEXTURE_SMOOTHING;
			input.backgroundFocusedSkin = backgroundFocusedSkin;
			
			input.minWidth = input.minHeight = (COMPONENT_SIZE * 1.1) << 0;
			input.minTouchWidth = input.minTouchHeight = COMPONENT_SIZE;
			input.paddingTop = input.paddingBottom = input.paddingLeft = input.paddingRight = 4 * Main.ASSET_SCALE_MULTIPLIER;
			
			input.textEditorProperties.fontFamily = "Helvetica";
			input.paddingTop = input.paddingBottom = 2 * Main.ASSET_SCALE_MULTIPLIER;
			input.textEditorProperties.fontSize = 18 * Main.ASSET_SCALE_MULTIPLIER;
			input.textEditorProperties.color = 0xffffff;
			input.textEditorProperties.textAlign = TextFormatAlign.CENTER;
		}
	}
}
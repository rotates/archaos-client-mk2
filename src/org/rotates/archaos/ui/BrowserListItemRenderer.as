package org.rotates.archaos.ui
{
	import feathers.controls.renderers.DefaultListItemRenderer;
	
	public class BrowserListItemRenderer extends DefaultListItemRenderer
	{
		public function BrowserListItemRenderer()
		{
			super();
		}
		
		public override function set data(value:Object):void
		{
			if(this._data == value)
			{
				return;
			}
			this._data = value;
			if (this._data.hasOwnProperty("waiting"){
				this.invalidate(INVALIDATION_FLAG_DATA);
			}
		}
	}
}
package org.rotates.archaos.ui
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.controls.TextInput;
	import feathers.data.ListCollection;
	import feathers.display.Scale9Image;
	import feathers.events.FeathersEventType;
	
	import org.rotates.archaos.Game;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	public class NewGame extends Screen
	{
		protected var theme:ArchaosTheme;
		protected var bg:Scale9Image;
		protected var gameName:TextInput;
		
		private const PADDING:uint = 3;
		
		private var _list:List;
		private var _header:Header;
		private var _back:Button;
		
		private var _gameName:TextInput;
		
		public function NewGame()
		{
			
		}
		
		protected override function initialize():void
		{
			owner.addEventListener(FeathersEventType.TRANSITION_START, onResize);
			stage.addEventListener(Event.RESIZE, onResize);
			
			theme = new ArchaosTheme(this.stage);
			
			_back = new Button();
			_back.nameList.add("button-back");
			_back.label = "Back";
			_back.addEventListener(Event.TRIGGERED, onBack);
			
			_header = new Header();
			_header.width = stage.stageWidth;
			_header.title = "New game";
			_header.leftItems = new <DisplayObject>[_back];
			
			addChild(_header);
			_header.validate();
			
			_gameName = new TextInput();
			_gameName.text = Game.handle + "'s game";
			_gameName.width = stage.stageWidth * 0.66;
			
			_list = new List();
			_list.nameList.add("list-unselectable");
			_list.isSelectable = false;
			_list.dataProvider = new ListCollection(
			[
				{ label: "Title", accessory: _gameName }	
			]);
			
			_list.height = stage.stageHeight - _header.height;
			_list.y = _header.height;
			
			addChild(_list);

			onResize();
		}
		
		private function onBack(e:Event = null):void
		{
			owner.showScreen(Game.BROWSER);
		}
		
		private function onResize(e:Event = null):void
		{
			if (Game.navigator.activeScreenID != this.screenID) {
				return;
			}

			_list.width = stage.stageWidth;
			_list.height = stage.stageHeight - _header.height;
			_header.width = stage.stageWidth;
			_gameName.width = stage.stageWidth * 0.66;
		}
	}
}
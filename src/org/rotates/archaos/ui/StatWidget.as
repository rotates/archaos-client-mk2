package org.rotates.archaos.ui
{
	import feathers.controls.Label;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.ui.LabelUtils;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class StatWidget extends Sprite
	{
		public static const MOVE:String = "move";		
		public static const FLY:String = "fly";
		public static const COMBAT:String = "combat";
		public static const DEFENSE:String = "defense";
		public static const RANGED_COMBAT:String = "ranged-combat";
		public static const RANGED_COMBAT_RANGE:String = "ranged-combat-range";
		public static const MAGIC_RESISTANCE:String = "magic-resist";
		public static const MANOEUVRE:String = "manoeuvre";
		
		public static const COLORS:Object = {
			"move": 0x54aa00,
			"fly": 0xe9e900,
			"combat": 0xf30000,
			"defense": 0x7a2200,
			"ranged-combat": 0xef5f00,
			"ranged-combat-range": 0xff6600,
			"magic-resist": 0xf325da,
			"manoeuvre": 0x618af5
		};
		
		private var _bg:Image = new Image(Game.ASSETS.getTexture("ui/stat-bg"));
		private var _number:Label;
		private var _icon:Image;
		private var _counter:Image;
		private var _type:String;
		private var _value:uint = 0;
		
		public function StatWidget(type:String = MOVE, value:uint = 1)
		{
			_type = type;
			_value = value;
			
			_icon = new Image(Game.ASSETS.getTexture("ui/stat-" + type));
			_counter = new Image(Game.ASSETS.getTexture("ui/stat-num-" + (Math.min(value, 10) || "1")));
			_counter.color = COLORS[_type];
			
			_bg.smoothing = _icon.smoothing = _counter.smoothing = Main.TEXTURE_SMOOTHING;
			
			_number = new Label();
			_number.x = 2 * Main.ASSET_SCALE_MULTIPLIER;
			_number.y = 16 * Main.ASSET_SCALE_MULTIPLIER;
			_number.width = 16 * Main.ASSET_SCALE_MULTIPLIER;
			_number.name = "centered";
			
			addChild(_bg);
			addChild(_icon);
			addChild(_counter);
			addChild(_number);
			
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			_counter.texture = Game.ASSETS.getTexture("ui/stat-num-" + (Math.min(_value, 10) || "1"));
			_number.text = _value.toString();
			_icon.texture = Game.ASSETS.getTexture("ui/stat-" + _type);
			_counter.color = COLORS[_type];
			LabelUtils.color(_number, COLORS[_type]);
			_counter.visible = (_value > 0);
			this.alpha = (_value < 1) ? 0.5 : 1;
			this.flatten();
		}
		
		public function set value(val:uint):void 
		{
			if (_value != val) {
				_value = val;
				_counter.texture = Game.ASSETS.getTexture("ui/stat-num-" + (Math.min(_value, 10) || "1"));
				_number.text = _value.toString();
				_counter.visible = (_value > 0);
				this.alpha = (_value < 1) ? 0.5 : 1;
				this.flatten();
			}
		}
		
		public function get value():uint
		{
			return _value;
		}
		
		public function set type(val:String):void 
		{
			if (_type != val) {
				_type = val;
				_icon.texture = Game.ASSETS.getTexture("ui/stat-" + _type);
				_counter.color = COLORS[_type];
				LabelUtils.color(_number, COLORS[_type]);
				this.flatten();
			}
		}
		
		public function get type():String
		{
			return _type;
		}
	}
}
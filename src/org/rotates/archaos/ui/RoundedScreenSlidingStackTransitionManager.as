package org.rotates.archaos.ui
{
	import feathers.controls.IScreen;
	import feathers.controls.ScreenNavigator;
	
	import flash.utils.getQualifiedClassName;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import feathers.motion.transitions.ScreenSlidingStackTransitionManager;
	
	public class RoundedScreenSlidingStackTransitionManager extends ScreenSlidingStackTransitionManager
	{
		public function RoundedScreenSlidingStackTransitionManager(navigator:ScreenNavigator, quickStack:Class=null)
		{
			super(navigator, quickStack);
		}
		
		protected override function onTransition(oldScreen:DisplayObject, newScreen:DisplayObject, onComplete:Function):void
		{
			if(!oldScreen || !newScreen)
			{
				if(newScreen)
				{
					newScreen.x = 0;
				}
				if(oldScreen)
				{
					oldScreen.x = 0;
				}
				onComplete();
				return;
			}
			
			if(this._activeTransition)
			{
				this._savedOtherTarget = null;
				Starling.juggler.remove(this._activeTransition);
				this._activeTransition = null;
			}
			
			this._savedCompleteHandler = onComplete;
			
			var newScreenClassAndID:String = getQualifiedClassName(newScreen);
			if(newScreen is IScreen)
			{
				newScreenClassAndID += "~" + IScreen(newScreen).screenID;
			}
			var stackIndex:int = this._stack.indexOf(newScreenClassAndID);
			var activeTransition_onUpdate:Function;
			if(stackIndex < 0)
			{
				var oldScreenClassAndID:String = getQualifiedClassName(oldScreen);
				if(oldScreen is IScreen)
				{
					oldScreenClassAndID += "~" + IScreen(oldScreen).screenID;
				}
				this._stack.push(oldScreenClassAndID);
				oldScreen.x = 0;
				newScreen.x = this.navigator.width;
				activeTransition_onUpdate = this.activeTransitionPush_onUpdate;
			}
			else
			{
				this._stack.length = stackIndex;
				oldScreen.x = 0;
				newScreen.x = -this.navigator.width;
				activeTransition_onUpdate = this.activeTransitionPop_onUpdate;
			}
			this._savedOtherTarget = oldScreen;
			this._activeTransition = new Tween(newScreen, this.duration, this.ease);
			this._activeTransition.animate("x", 0);
			this._activeTransition.roundToInt = true;
			this._activeTransition.delay = this.delay;
			this._activeTransition.onUpdate = activeTransition_onUpdate;
			this._activeTransition.onComplete = activeTransition_onComplete;
			Starling.juggler.add(this._activeTransition);
		}
	}
}
package org.rotates.archaos.ui
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	import feathers.display.TiledImage;
	
	import flash.geom.Point;
	import flash.system.Capabilities;
	
	import org.gestouch.core.GestureState;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.PanGesture;
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.renderers.IBoardRenderer;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class ActiveGame extends Screen
	{
		private static const PADDING:uint = 6;
		protected var theme:ArchaosTheme;
		
		private var _background:TiledImage;
		private var _menu:Button;
		private var _board:Board;
		private var _boardRenderer:IBoardRenderer;
		private var _boardRendererOffset:Point = new Point(0,0);
		private var _canvas:Sprite = new Sprite();
		private var _pan:PanGesture;
		private var _scale:Number = 1.5;
		
		public function ActiveGame()
		{
			
		}
		
		
		protected override function initialize():void
		{			
			owner.addEventListener(Event.CHANGE, onOpen);
			stage.addEventListener(Event.RESIZE, onResize);
			
			theme = new ArchaosTheme(this.stage);
			
			const menuIcon:Image = new Image(Game.ASSETS.getTexture("ui/icon-menu"));
			menuIcon.smoothing = Main.TEXTURE_SMOOTHING;
			
			_menu = new Button();
			_menu.defaultIcon = menuIcon;
			_menu.addEventListener(Event.TRIGGERED, onMenu);
			_menu.name = Header.DEFAULT_CHILD_NAME_ITEM;
			_menu.y = PADDING;
			
			_background = new TiledImage(Game.ASSETS.getTexture("ui/game-background"));
			_background.smoothing = Main.TEXTURE_SMOOTHING;
			
			_pan = new PanGesture(_canvas);
			_pan.addEventListener(GestureEvent.GESTURE_STATE_CHANGE, onPan);
			
			this.addChild(_background);
			this.addChild(_canvas);
			this.addChild(_menu);

			onResize();
		}
		
		private function onPan(e:GestureEvent):void
		{
			switch (e.newState)
			{
				case GestureState.BEGAN:
					_boardRendererOffset.x = _pan.location.x - (_canvas.x) * 2;
					_boardRendererOffset.y = _pan.location.y - (_canvas.y) * 2;
					break;
				case GestureState.CHANGED:
					_canvas.x = (_pan.location.x - _boardRendererOffset.x) * 0.5;
					_canvas.y = (_pan.location.y - _boardRendererOffset.y) * 0.5;
					clampCanvas();
					break;
			}
		}
		
		private function clampCanvas():void
		{
			if (stage.stageWidth > _canvas.width) {
				_canvas.x = stage.stageWidth * 0.5;
			}
			else if (_canvas.x > (_canvas.width * 0.5 + 14)) {
				_canvas.x = (_canvas.width * 0.5 + 14);
			}
			else if (_canvas.x < (_canvas.width * 0.5 - stage.stageWidth + 14) * -1) {
				_canvas.x = (_canvas.width * 0.5 - stage.stageWidth + 14) * -1	
			}
			
			if (stage.stageHeight - 14 > _canvas.height) {
				_canvas.y = stage.stageHeight * 0.5;
			}
			else if (_canvas.y > _canvas.height * 0.5 + 14) {
				_canvas.y = _canvas.height * 0.5 + 14;
			}
			else if (_canvas.y < (_canvas.height * 0.5 - stage.stageHeight + 14) * -1) {
				_canvas.y = (_canvas.height * 0.5 - stage.stageHeight + 14) * -1;
			}
		}
		
		private function onMenu(e:Event = null):void
		{
			Game.setActiveBoard(null);
			owner.showScreen(Game.BROWSER);
		}
		
		private function onOpen(e:Event = null):void
		{
			if (Game.navigator.activeScreenID != this.screenID) {
				return;
			}
			if (_boardRenderer != null) {
				_canvas.removeChild(_boardRenderer.displayObject);
			}
			_board = Game.getActiveBoard();
			_boardRenderer = _board.renderers[1];
			_canvas.addChild(_boardRenderer.displayObject);
			
			_boardRendererOffset.x = _boardRendererOffset.y = 0;
			_boardRenderer.displayObject.y = stage.stageHeight + _boardRenderer.displayObject.height + 14;	
			_boardRenderer.displayObject.scaleX = _boardRenderer.displayObject.scaleY = _scale;
			Starling.juggler.tween(_boardRenderer.displayObject, 1, {roundToInt: true, transition: Transitions.EASE_OUT, delay: 0.5, y: 0 });
			
			onResize();
			if (_board.currentPlayer != null) {
				_background.color = Game.PLAYER_COLORS[_board.currentPlayer.index];
			}
			else {
				_background.color = 0x444444;
			}
		}
		
		private function onResize(e:Event = null):void
		{
			if (Game.navigator.activeScreenID != this.screenID) {
				return;
			}
			_menu.x = PADDING;
			
			_canvas.x = (stage.stageWidth * 0.5 + _boardRendererOffset.x) << 0;
			_canvas.y = (stage.stageHeight * 0.5 + _boardRendererOffset.y) << 0;
			
			_background.width = stage.stageWidth;
			_background.height = stage.stageHeight;
			clampCanvas();
		}
	}
}
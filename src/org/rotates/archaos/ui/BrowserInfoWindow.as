package org.rotates.archaos.ui
{
	import ca.revoke.utils.Tint;
	
	import feathers.controls.Label;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Board;
	import org.rotates.archaos.board.renderers.IBoardRenderer;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.ClippedSprite;
	import starling.textures.TextureSmoothing;
	
	public class BrowserInfoWindow extends Sprite
	{

		private var _bg:Scale9Image;
		private var _title:Label;
		private var _subtitle:Label;
		private var _message:Label;
		private var _logo:Image;
		private var _minimap:IBoardRenderer;
		private var _starburst:Image;
		
		private var _canvas:ClippedSprite;
		private var _clipRect:Rectangle;
		
		private const PADDING:uint = 3 * Main.ASSET_SCALE_MULTIPLIER;
		
		public function BrowserInfoWindow()
		{
			_bg = new Scale9Image(new Scale9Textures(Game.ASSETS.getTexture("ui/callout-disabled"), ArchaosTheme.CALLOUT_GRID));
			_bg.smoothing = Main.TEXTURE_SMOOTHING;
			addChild(_bg);
			
			_canvas = new ClippedSprite();
			_clipRect = new Rectangle(this.x + PADDING, this.y + PADDING, this.width - (PADDING * 2), this.height - (PADDING * 2));
			_canvas.clipRect = _clipRect;
			addChild(_canvas);
			
			_starburst = new Image(Game.ASSETS.getTexture("ui/starburst"));
			_starburst.smoothing = Main.TEXTURE_SMOOTHING;
			_starburst.pivotX = (_starburst.width * 0.5) << 0;
			_starburst.pivotY = (_starburst.height * 0.5) << 0;
			_starburst.color = 0x222222;
			_canvas.addChild(_starburst);

			_logo = new Image(Game.ASSETS.getTexture("ui/logo"));
			_logo.smoothing = Main.TEXTURE_SMOOTHING;
			_logo.pivotX = (_logo.width * 0.5) << 0;
			_logo.pivotY = (_logo.height * 0.5) << 0;
			_canvas.addChild(_logo);
			
			_title = new Label();
			_title.nameList.add("big-centered");
			_canvas.addChild(_title);
			
			_subtitle = new Label();
			_subtitle.nameList.add("centered");
			_canvas.addChild(_subtitle);
			
			_message = new Label();
			_message.nameList.add("centered");
			_message.text = "Loading games, please wait...";
			_canvas.addChild(_message);
			
			_message.x = _subtitle.x = _title.x = PADDING * 2;
			
			onResize();
			
			addEventListener(Event.RESIZE, onResize);
		}
		
		public function updateMessage(text:String):void
		{
			Starling.juggler.tween(_message, 0.25, { alpha: 0, onComplete: function():void {
				_message.text = text;	
				Starling.juggler.tween(_message, 0.25, { alpha: 1 });
			}});
		}
		
		private function updateSubtitle(board:Board):void
		{
			var newSubtitle:Array = new Array();
			if (board.active == true) {
				newSubtitle.push("Active");
				newSubtitle.push(board.players.length + ((board.players.length == 1) ? " player" : " players"));
			}
			else {
				if (board.ended == true) {
					newSubtitle.push("Ended");
					newSubtitle.push(board.players.length + ((board.players.length == 1) ? " player" : " players"));
				}
				else {
					newSubtitle.push("Waiting for players");
					newSubtitle.push(board.players.length + "/" + board.maxPlayers + " players");
				}
				
			}
			if (board.round > 0) {
				newSubtitle.push("round " + board.round);
			}
			if (board.currentPlayer) {
				newSubtitle.push(board.currentPlayer.handle + "'s turn");
			}
			_subtitle.text = newSubtitle.join(", ");
			_subtitle.validate();
			onResize();
		}
		
		public override function get width():Number
		{
			return _bg.width;
		}
		
		public override function get height():Number
		{
			return _bg.height;
		}
		
		public override function set width(val:Number):void
		{
			_bg.width = val;
			onResize();
		}
		
		public override function set height(val:Number):void
		{
			_bg.height = val;
			onResize();
		}
		
		private function onResize(e:Event = null):void {
			_message.y = _bg.height - (ArchaosTheme.LARGE_FONT_SIZE) - (PADDING * 2);
			_subtitle.y = (_title.height === 0) ? (ArchaosTheme.LARGE_FONT_SIZE) + (PADDING * 2) : _title.height + (PADDING * 2);
			
			_message.width = _title.width = _subtitle.width = (Math.round((_bg.width - PADDING * 4)/2) * 2) << 0;
			
			_title.validate();
			_subtitle.validate();

			_logo.x = (_bg.width * 0.5) << 0;
			_logo.y = (_bg.height * 0.5) << 0;
			
			_starburst.x = (_bg.width * 0.5) << 0;
			if (_logo.visible) {
				_starburst.y = (_bg.height * 0.5) << 0;
			}
			else {
				_starburst.y = (_bg.height * 0.55) << 0;
			}
			
			if (_minimap != null) {
				_minimap.displayObject.x = (_bg.width * 0.5) << 0;
				_minimap.displayObject.y = (_bg.height * 0.5) + 8 << 0;
			}
			
			_clipRect.x = this.x + PADDING;
			_clipRect.y = this.y + PADDING;
			
			_clipRect.width = _bg.width - (PADDING * 2);
			_clipRect.height = _bg.height - (PADDING * 2);
			
			_canvas.clipRect = _clipRect;
		}
		
		public function update(board:Board):void
		{
			if (_minimap != null && _canvas.contains(_minimap.displayObject)) {
				Starling.juggler.tween(_minimap.displayObject, 0.5, { roundToInt: true, transition: Transitions.EASE_IN, x: _bg.width * -1, onComplete: function():void {
					_canvas.removeChild(_minimap.displayObject);
					_minimap = board.renderers[0];
					_minimap.displayObject.x = (_bg.width * 1.5);
					_minimap.displayObject.y = (_bg.height * 0.5) + 8 << 0;
					_canvas.addChildAt(_minimap.displayObject, 3);
					Starling.juggler.tween(_minimap.displayObject, 0.5, {roundToInt: true, transition: Transitions.EASE_OUT, x: (_bg.width * 0.5) << 0});
				}});
			}
			else {
				_minimap = board.renderers[0];
				_minimap.displayObject.x = (_bg.width * 1.5);
				_minimap.displayObject.y = (_bg.height * 0.5) + 8 << 0;
				_canvas.addChildAt(_minimap.displayObject, 3);
				Starling.juggler.tween(_minimap.displayObject, 0.5, { roundToInt: true, transition: Transitions.EASE_OUT, x: (_bg.width * 0.5) << 0});				
			}
			Starling.juggler.tween(_starburst, 0.5, { alpha: 0, onComplete: function():void {
				if (board.currentPlayer != null) {
					_starburst.color = Tint.darken(Game.PLAYER_COLORS[board.currentPlayer.index],0.8);;
				}
				else {
					_starburst.color = 0x171717;
				}
				Starling.juggler.tween(_starburst, 0.5, { alpha: 1 });
			}});
			
			if (_logo.alpha > 0 || _logo.visible) {
				Starling.juggler.tween(_logo, 0.5, { transition: Transitions.EASE_IN, alpha: 0, y: _logo.y + 10, onComplete: function():void { _logo.visible = false; } });
				Starling.juggler.tween(_starburst, 1, {roundToInt: true, transition: Transitions.EASE_IN_OUT, y: (_bg.height * 0.55) << 0 });
			}
			
			Starling.juggler.tween(_subtitle, 0.5, { alpha: 0 });
			Starling.juggler.tween(_title, 0.5, {roundToInt: true, transition: Transitions.EASE_IN, y: (ArchaosTheme.LARGE_FONT_SIZE * -1), onComplete: function():void {
				_title.text = board.name;
				_title.validate();
				Starling.juggler.tween(_title, 0.5, {roundToInt: true, transition: Transitions.EASE_OUT, y: ((PADDING * 2) * Main.ASSET_SCALE_MULTIPLIER), onComplete: function():void {
					updateSubtitle(board);
					Starling.juggler.tween(_subtitle, 0.5, { alpha: 1 });
				}});
				updateMessage((Main.MOBILE ? "Tap" : "Click") + " the board to play");
			}});
		}
	}
}
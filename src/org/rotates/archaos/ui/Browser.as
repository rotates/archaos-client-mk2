package org.rotates.archaos.ui
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.data.ListCollection;
	import feathers.events.FeathersEventType;
	
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Board;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;

	
	public class Browser extends Screen
	{
		private var theme:ArchaosTheme;
		private var _newGame:Button;
		private var _settings:Button;
		private var _list:List;
		private var _header:Header;
		private var _listData:ListCollection;
		private var _infoWindow:BrowserInfoWindow;
		private var _tapGesture:TapGesture;
		
		public function Browser()
		{
			
		}
		
		public function refresh():void
		{
			var data:Object, board:Board;
			Game.NET.client.getGames(function(data:Object):void {
				if (data.response.hasOwnProperty("success")) {
					for (var b:String in data.response.success) {
						board = Game.addBoard(data.response.success[b].id, data.response.success[b]);
						if (data.response.success[b].hasOwnProperty("currentPlayer")) {
							board.setCurrentPlayer(data.response.success[b].currentPlayer);
						}
					}
					_listData = new ListCollection(formatBoards());
					
					_list.dataProvider = _listData;
					
					_list.itemRendererProperties.labelField = "name";
					_list.itemRendererProperties.accessoryLabelField = "turn";
					
					_list.addEventListener(Event.CHANGE, selectHandler);
					
					updateInfoWindow();
				}
			});
		}
		
		protected override function initialize():void
		{
			owner.addEventListener(FeathersEventType.TRANSITION_START, onResize);
			
			stage.addEventListener(Event.RESIZE, onResize);
			
			theme = new ArchaosTheme(this.stage);
			
			_settings = new Button();
			_newGame = new Button();
			
			_settings.nameList.add(Header.DEFAULT_CHILD_NAME_ITEM);
			_newGame.nameList.add("button-forward");
			
			const settingsIcon:Image = new Image(Game.ASSETS.getTexture("ui/icon-settings"));
			const newGameIcon:Image = new Image(Game.ASSETS.getTexture("ui/icon-add"));
			
			settingsIcon.smoothing = newGameIcon.smoothing = Main.TEXTURE_SMOOTHING;
			
			_settings.defaultIcon = settingsIcon;
			_newGame.defaultIcon = newGameIcon;
			_newGame.iconPosition = Button.ICON_POSITION_RIGHT;
			
			_newGame.label = "New game";
			_newGame.addEventListener(Event.TRIGGERED, onNewGame);
			
			_header = new Header();
			_header.width = stage.stageWidth;
			if (Main.MOBILE === false) {
				_header.title = "Games";
			}
			_header.leftItems = new <DisplayObject>[_settings];
			_header.rightItems = new <DisplayObject>[_newGame];
			
			_infoWindow = new BrowserInfoWindow();
			_infoWindow.useHandCursor = true;
			
			_tapGesture = new TapGesture(_infoWindow);
			_tapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTap);
			
			_list = new List();
			_list.useHandCursor = true;
			_list.scrollerProperties.clipContent = false;
			
			this.addChild(_list);
			this.addChild(_newGame);
			this.addChild(_infoWindow);
			this.addChild(_header);
			_header.validate();
			
			onResize();
		}
		
		private function onTap(e:GestureEvent):void
		{
			var board:Board;
			if (_list.selectedIndex != -1) {
				board = Game.boards[_list.selectedIndex];
				Game.setActiveBoard(board.id);
				owner.showScreen(Game.ACTIVEGAME);
			}
		}
		
		private function onNewGame(e:Event = null):void
		{
			owner.showScreen(Game.NEWGAME);
		}
		
		private function formatBoards():Array
		{
			var output:Array = new Array();
			var turnStatus:String;
			var waitingStatus:Boolean;
			
			if (Game.boards.length > 0) {
				for (var b:uint = 0; b < Game.boards.length; b++) {
					
					if (Game.boards[b].round > 0 ) {
						if (Game.boards[b].active == false) {
							turnStatus = "Ended";
						}
						else {
							turnStatus = Game.boards[b].round.toString() + " " + ((Game.boards[b].currentPlayer.username == Game.username) ? "(your turn)" : "");
							waitingStatus = ((Game.boards[b].currentPlayer.username == Game.username) ? true : false)
						}
					}
					else {
						turnStatus = "Lobby";
					}
					
					output.push({name: Game.boards[b].name, turn: turnStatus, id: Game.boards[b].id, waiting: waitingStatus});
				}
				return output;
			}
			return null;
		}
		
		private function init(e:Event = null):void
		{	

		}
		
		private function onResize(e:* = null):void
		{
			if (Game.navigator.activeScreenID != this.screenID) {
				return;
			}
			const INFO_HEIGHT:uint = Math.max(stage.stageHeight * 0.5, 140);
			const INFO_WIDTH:uint = Math.max(stage.stageWidth * 0.5, 140);
			
			_header.width = stage.stageWidth;
			
			_infoWindow.y = _header.height;
			
			if (stage.stageWidth >= stage.stageHeight)
			{
				_infoWindow.width = INFO_WIDTH;
				_infoWindow.height = stage.stageHeight - _header.height;
				_list.x = INFO_WIDTH;
				_list.y = _header.height;
				_list.width = stage.stageWidth - INFO_WIDTH;
				_list.height = stage.stageHeight - _header.height;

			}
			else
			{
				_infoWindow.width = stage.stageWidth;
				_infoWindow.height = INFO_HEIGHT;
				_list.x = 0;
				_list.y = INFO_HEIGHT + _header.height;
				_list.width = stage.stageWidth;
				_list.height = stage.stageHeight - INFO_HEIGHT - _header.height;
			}
		}

		private function updateInfoWindow():void
		{
			if (Game.boards.length > 0) {
				_infoWindow.updateMessage("Select or create a game");
			}
			else {
				_infoWindow.updateMessage("Create a new game");
			}
		}
		
		private function selectHandler(e:Event):void
		{
			if (_list.selectedItem.id > 0) {
				_infoWindow.updateMessage("Loading board, please wait...");
				var board:Board = Game.getBoard(_list.selectedItem.id.toString());
				board.update(function():void {
					_infoWindow.update(board);
					Game.setActiveBoard(board.id);
				});
			}
		}
	}
}
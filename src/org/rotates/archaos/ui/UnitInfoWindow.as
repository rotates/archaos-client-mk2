package org.rotates.archaos.ui
{
	import feathers.controls.Label;
	import feathers.display.TiledImage;
	
	import org.rotates.archaos.Game;
	import org.rotates.archaos.board.Unit;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	public class UnitInfoWindow
	{
		public static const WINDOW_WIDTH:uint = 140 * Main.ASSET_SCALE_MULTIPLIER;
		public static const WINDOW_HEIGHT:uint = 48 * Main.ASSET_SCALE_MULTIPLIER;
		
		private static var bg:Quad = new Quad(WINDOW_WIDTH, WINDOW_HEIGHT, 0x0);
		private static var canvas:Sprite = new Sprite();
		private static var title:Label = new Label();
		private static var ready:Boolean = false;
		
		private static var _statWidgets:Object = {
			"mov": new StatWidget(StatWidget.MOVE),
			"com": new StatWidget(StatWidget.COMBAT),
			"rcm": new StatWidget(StatWidget.RANGED_COMBAT),
			"rng": new StatWidget(StatWidget.RANGED_COMBAT_RANGE),
			"def": new StatWidget(StatWidget.DEFENSE),
			"mnv": new StatWidget(StatWidget.MANOEUVRE),
			"res": new StatWidget(StatWidget.MAGIC_RESISTANCE)
		};
		
		private static var _statWidgetsOrder:Vector.<String> = Vector.<String>(["mov", "com", "rcm", "rng", "def", "res", "mnv"]);
		
		public function UnitInfoWindow()
		{
			
		}
		
		public static function init():void
		{
			bg.visible = false;
			canvas.addChild(bg);
			
			title.width = WINDOW_WIDTH;
			title.name = "centered";		
			var xx:uint = 0;
			
			for each (var stat:String in _statWidgetsOrder) {
				_statWidgets[stat].y = 18 * Main.ASSET_SCALE_MULTIPLIER;
				_statWidgets[stat].x = xx * Main.ASSET_SCALE_MULTIPLIER;
				xx += 20;
				canvas.addChild(_statWidgets[stat]);
			}
			
			canvas.addChild(title);
			
			ready = true;
		}
		
		public static function create(unit:Unit):Sprite
		{
			if (!ready) {
				init();
			}
			var unitDef:Object = Game.unitDefs[unit.type];
			if (unitDef !== null) {
				title.text = unitDef.name + " (" + (unit.moved ? "M" : "-") + (unit.attacked ? "A" : "-") + (unit.rangedAttacked ? "R" : "-") + (unit.engaged ? "E" : "-") + ")";
				title.validate();
				
				unit.canFly() ? _statWidgets["mov"].type = StatWidget.FLY : _statWidgets["mov"].type = StatWidget.MOVE;
				
				for each (var stat:String in _statWidgetsOrder) {
					_statWidgets[stat].value = unit.properties[stat];
				}
			}
			return canvas;
		}
	}
}
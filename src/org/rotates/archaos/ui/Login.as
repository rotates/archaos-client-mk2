package org.rotates.archaos.ui
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Screen;
	import feathers.controls.TextInput;
	import feathers.data.ListCollection;
	import feathers.display.Scale9Image;
	import feathers.display.TiledImage;
	import feathers.events.FeathersEventType;
	import feathers.layout.VerticalLayout;
	import feathers.text.BitmapFontTextFormat;
	import feathers.textures.Scale9Textures;
	
	import flash.geom.Rectangle;
	
	import org.rotates.archaos.Game;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureSmoothing;
	
	public class Login extends Screen
	{
		protected var theme:ArchaosTheme;
		private var _bg:Scale9Image;
		private var _mainBg:TiledImage;
		
		private const PLACEHOLDER_ALPHA:Number = 0.3;
		
		private const PADDING:uint = 3;
		
		private var _username:TextInput;
		private var _password:TextInput;
		private var _login:Button;
		private var _loginBox:Sprite;
		private var _message:Label;
		
		public function Login()
		{
			
		}
		
		protected override function initialize():void
		{			
			owner.addEventListener(FeathersEventType.TRANSITION_START, onResize);
			stage.addEventListener(Event.RESIZE, onResize);
			
			theme = new ArchaosTheme(this.stage);
			
			_mainBg = new TiledImage(Game.ASSETS.getTexture("ui/background"));
			_mainBg.smoothing = Main.TEXTURE_SMOOTHING;
			addChild(_mainBg);
			
			_loginBox = new Sprite();
			_message = new Label();
			_message.nameList.add("centered");
			_message.text = "Enter your username and password below";
			
			_bg = new Scale9Image(new Scale9Textures(Game.ASSETS.getTexture("ui/callout-disabled"), new Rectangle(2,2,1,1)));
			_bg.smoothing = Main.TEXTURE_SMOOTHING;
			_loginBox.addChild(_bg);
			
			_username = new TextInput();
			_password = new TextInput();
			_username.name = _password.name = "big";
			
			_username.text = "Username";
			_password.text = "Password";
			_username.textEditorProperties.alpha = _password.textEditorProperties.alpha = PLACEHOLDER_ALPHA;
			
			_username.addEventListener(FeathersEventType.FOCUS_IN, checkPlaceholder);
			_username.addEventListener(FeathersEventType.FOCUS_OUT, checkPlaceholder);
			_password.addEventListener(FeathersEventType.FOCUS_IN, checkPlaceholder);
			_password.addEventListener(FeathersEventType.FOCUS_OUT, checkPlaceholder);
			
			_login = new Button();
			_login.label = "Login";
			_login.isEnabled = false;
			
			_loginBox.addChild(_username);
			_loginBox.addChild(_password);
			_loginBox.addChild(_login);
			_loginBox.addChild(_message);
			addChild(_loginBox);
			
			_login.addEventListener(Event.TRIGGERED, onLogin);
			
			_loginBox.x = PADDING;
			_message.x = PADDING * 2;

			onResize();
		}
		
		private function onLogin(e:Event):void
		{
			Game.doLogin(_username.text, _password.text);
		}
		
		public function showError(message:String):void
		{
			LabelUtils.color(_message, 0xff0000);
			_message.text = "Error: " + message;
		}
		
		private function checkPlaceholder(e:Event):void
		{
			if (e.target == _username) {
				if (_username.text == "Username") {
					_username.text = "";
					_login.isEnabled = true;
					_username.textEditorProperties.alpha = 1;
				}
				else if (_username.text == "") {
					_username.text = "Username";
					_login.isEnabled = false;
					_username.textEditorProperties.alpha = PLACEHOLDER_ALPHA;
				}
				else {
					_login.isEnabled = true;
					_username.textEditorProperties.alpha = 1;
				}
			}
			if (e.target == _password) {
				if (_password.text == "Password") {
					_password.text = "";
					_password.textEditorProperties.displayAsPassword = true;
					_login.isEnabled = true;
					_password.textEditorProperties.alpha = 1;
				}
				else if (_password.text == "") {
					_password.text = "Password";
					_password.textEditorProperties.displayAsPassword = false;
					_login.isEnabled = false;
					_password.textEditorProperties.alpha = PLACEHOLDER_ALPHA;
				}
				else {
					_password.textEditorProperties.displayAsPassword = true;
					_login.isEnabled = true;
					_password.textEditorProperties.alpha = 1;
				}
			}
		}
		
		private function onResize(e:Event = null):void
		{
			if (Game.navigator.activeScreenID != this.screenID) {
				return;
			}
			_username.validate();
			_password.validate();
			_login.validate();
			_message.validate();
			
			_username.x = _password.x = _login.x = PADDING * 2;
			
			_message.y = PADDING * 2;
			_username.y = _message.y + _message.height + PADDING * 2;
			_password.y = _username.y + _username.height + PADDING;
			_login.y = _password.y + _username.height + PADDING;
			_bg.height = _login.y + _login.height + PADDING * 2;
			
			_username.width = _password.width = _login.width = stage.stageWidth - PADDING * 6;
			_bg.width = stage.stageWidth - PADDING * 2;
			_loginBox.y = (stage.stageHeight - _loginBox.height - PADDING) >> 1;
			_message.width = (Math.round((_bg.width - PADDING * 4)/2) * 2) << 0;
			_mainBg.width = stage.stageWidth;
			_mainBg.height = stage.stageHeight;
		}
	}
}
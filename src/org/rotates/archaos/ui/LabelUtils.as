package org.rotates.archaos.ui
{
	import feathers.controls.Label;
	import feathers.text.BitmapFontTextFormat;
	
	public class LabelUtils
	{
		public function LabelUtils()
		{
		}
		
		public static function color(label:Label, col:uint):void
		{
			if (label && col) {
				var tf:BitmapFontTextFormat = label.textRendererProperties.textFormat;
				if (tf) {
					label.textRendererProperties.textFormat = null;
					tf.color = col;
					label.textRendererProperties.textFormat = tf;
				}
			}
		}
	}
}